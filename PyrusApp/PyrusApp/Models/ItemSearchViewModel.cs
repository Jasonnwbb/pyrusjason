﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace PyrusApp.Models
{
    public class ItemSearchViewModel
    {
        public List<SelectListItem> Cities { get; set; }
        public List<SelectListItem> Categories { get; set; }
        public CraigslistSearch Search { get; set; }
    }
}