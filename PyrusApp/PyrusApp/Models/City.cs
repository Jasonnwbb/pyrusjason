namespace PyrusApp.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class City
    {
        public int ID { get; set; }

        [Required]
        [StringLength(255)]
        public string CityName { get; set; }

        [Required]
        [StringLength(255)]
        public string StateName { get; set; }

        [Required]
        [StringLength(255)]
        public string Extension { get; set; }

        public int ZipCode { get; set; }
    }
}
