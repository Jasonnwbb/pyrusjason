﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PyrusApp.Models
{
    public class CraigslistAd
    {
        public string Title { get; set; }

        public string City { get; set; }

        public string Category { get; set; }

        public string URL { get; set; }

        public string SmallImageURL { get; set; }

        public string ThumbnailImageURL { get; set; }

        public string MediumImageURL { get; set; }

        public string BigImageURL { get; set; }
        
        public string TimeStamp { get; set; }

        public string PartialDescription { get; set; }

        public string Notes { get; set; }

        [Display(Name="Price")]
        public int? AskingPrice { get; set; }

        public override int GetHashCode()
        {
            int returnValue = 1;

            //179426549 is prime
            foreach (int c in URL)
            {
                returnValue = (c * returnValue) % 179426549;
            }

            return returnValue;

        }


        public override string ToString()
        {
            StringBuilder builder = new StringBuilder();
            builder.Append("Title:\t\t\t" +Title +"\n");
            if(AskingPrice != null)
            {
                builder.Append("Asking Price:\t\t" +"$" +AskingPrice + "\n");
            }
            
            builder.Append("URL:\t\t\t" + URL + "\n");
            builder.Append("TimeStamp:\t\t" + TimeStamp + "\n\n");
            if(SmallImageURL != null)
            {
                builder.Append("SmallImageURL:\t\t" + SmallImageURL + "\n");
                builder.Append("MediumImageURL:\t\t" + MediumImageURL + "\n");
                builder.Append("ThumbnailImageURL:\t" + ThumbnailImageURL + "\n");
                builder.Append("BigImageURL:\t\t" + BigImageURL + "\n\n");

            }
            builder.Append("Partial Description:\n" + PartialDescription + "\n");
            return builder.ToString();
        }

    }
}