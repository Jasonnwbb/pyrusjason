﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using System.Text.RegularExpressions;
using System.Web.Mvc;

namespace PyrusApp.Models
{
    public class JobSearch
    {
        /*
        public enum JobType
        {
            All,
            FullTime,
            PartTime,
            Contract,
            Internship,
            Temporary
        }
        */
        //[Required]
        [Display(Name ="Search:")]
        public string Query { get; set; }

        [Required]
        [Display(Name ="Cities:")]
        public List<int> CityIDs { get; set; }

        public List<City> Cities { get; set; }

    }
}