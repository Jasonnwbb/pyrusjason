namespace PyrusApp.Models
{
    using System;
    using System.Data.Entity;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Linq;

    public partial class CraigslistContext : DbContext
    {
        public CraigslistContext()
            : base("Pyrus")
        {
            //IdentityDbContext identityDbContext = new IdentityDbContext();
        }

        public virtual DbSet<Category> Categories { get; set; }
        public virtual DbSet<City> Cities { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Category>()
                .Property(e => e.CategoryName)
                .IsUnicode(false);

            modelBuilder.Entity<Category>()
                .Property(e => e.Extension)
                .IsUnicode(false);

            modelBuilder.Entity<City>()
                .Property(e => e.CityName)
                .IsUnicode(false);

            modelBuilder.Entity<City>()
                .Property(e => e.StateName)
                .IsUnicode(false);

            modelBuilder.Entity<City>()
                .Property(e => e.Extension)
                .IsUnicode(false);
        }
    }
}
