﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Web;
using System.Xml;
using System.Xml.Linq;

namespace PyrusApp.Models
{
    
    
    public static class IndeedUtilities
    {
        public static List<LocationAndURL> BuildJobSearchBaseURlsWithLocations(JobSearch jobSearch, HttpRequestBase curRequest, int pages)
        {
            StringBuilder templateURL = new StringBuilder();
            templateURL.Append("http://api.indeed.com/ads/apisearch?publisher=5928923467765024&limit=25&q=" +jobSearch.Query +"&userip=");
            templateURL.Append(curRequest.UserHostAddress);
            templateURL.Append("&useragent=");
            templateURL.Append(curRequest.UserAgent);
            templateURL.Append("&v=2");
            templateURL.Append("&l=");

            var locationsAndURLs = new List<LocationAndURL>();

            foreach (var city in jobSearch.Cities)
            {
                for(int i = 0; i < pages; i++)
                {
                    locationsAndURLs.Add(new LocationAndURL { URL = templateURL.ToString() + city.ZipCode.ToString() + "&start=" +i, Location = city.CityName });
                }
                
            }

            return locationsAndURLs;

        }

        public static int TotalNumResults(string URL)
        {
            XElement indeedFeed;
            int totalNumResults;
            try
            {
                WebResponse webResponse = WebRequest.Create(URL).GetResponse();
                StreamReader responseStream = new StreamReader(webResponse.GetResponseStream());
                indeedFeed = XElement.Parse(RemoveInvalidXmlChars(responseStream.ReadToEnd()));
                totalNumResults = int.Parse(indeedFeed.Descendants().Where(element => element.Name == "totalresults").First().Value);
            }

            //Catch a WebException from the webResponse or InvalidOperationException from the parse.
            catch (Exception exception)
            {
                return 0;
            }

            return totalNumResults;
        }


        public static IEnumerable<XElement> Results(string URL)
        {
            XElement indeedFeed;
            try
            {
                WebResponse webResponse = WebRequest.Create(URL).GetResponse();
                StreamReader responseStream = new StreamReader(webResponse.GetResponseStream());
                indeedFeed = XElement.Parse(RemoveInvalidXmlChars(responseStream.ReadToEnd()));
            }

            catch(WebException exception)
            {
                //return an empty list if the web response fails
                return new List<XElement>();
            }

            return indeedFeed.Elements().Where(element => element.Name == "results").Elements();
        }

        public static AdType parseResult(XElement result, string Location)
        {
            AdType ad;
            try
            {
                ad = new AdType
                {
                    Title = result.Elements().Where(element => element.Name == "jobtitle").First().Value,
                    Network = "Indeed",
                    Description = result.Elements().Where(element => element.Name == "snippet").First().Value,
                    URL = result.Elements().Where(element => element.Name == "url").First().Value,
                    Location = Location,
                    Date = result.Elements().Where(element => element.Name == "date").First().Value
                };
            }

            catch (InvalidOperationException exception)
            {
                throw exception;
            }

            return ad;
        }

        private static string RemoveInvalidXmlChars(string text)
        {
            if (text == null || text.Length == 0)
            {
                return text;
            }

            StringBuilder result = new StringBuilder();
            for (int i = 0; i < text.Length; i++)
            {
                if (XmlConvert.IsXmlChar(text[i]))
                {
                    result?.Append(text[i]);
                }
            }

            return result.ToString();
        }
    }
    
}