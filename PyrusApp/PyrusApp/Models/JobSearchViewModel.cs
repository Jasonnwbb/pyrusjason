﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace PyrusApp.Models
{
    public class JobSearchViewModel
    {
        public JobSearch JobSearch { get; set; }
        public MultiSelectList Cities { get; set; }
    }
}