﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PyrusApp.Models
{
    public class AdType
    {
        public string Title { get; set; }
        public string Network { get; set; }
        public string Description { get; set; }
        public string URL { get; set; }
        public string Location { get; set; }
        public string Date {get; set;}
    }
}
