﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace PyrusApp.Models
{
    public class CraigslistSearch
    {
        [Required(ErrorMessage = "You must select at least one city.")]
        [MinLength(1)]
        [MaxLength(4, ErrorMessage = "You cannot select more than four cities.")]
        public int[] Cities{ get; set; }

        [Required]
        [MinLength(1, ErrorMessage = "You must select at least one category.")]
        public int[] Categories { get; set; }

        [Display(Name ="Search Terms")]
        public string Query { get; set; }

        [Display(Name = "Image Only")]
        public bool HasImage { get; set; }

        public int? MinPrice { get; set; }

        public int? MaxPrice { get; set; }
    }
}