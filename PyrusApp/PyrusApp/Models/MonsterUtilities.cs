﻿using PyrusApp.Models;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Web;
using System.Xml;
using System.Xml.Linq;

namespace PyrusApp.Models
{
    
    public class MonsterUtilities
    {
        public static List<LocationAndURL> BuildJobSearchBaseURlsWithLocations(JobSearch jobSearch, int pages)
        {
            StringBuilder templateURL = new StringBuilder();
            templateURL.Append("http://rss.jobsearch.monster.com/rssquery.ashx?tm=30&q=" + jobSearch.Query +"&where=");
            List<LocationAndURL> locationsAndURLs = new List<LocationAndURL>();

            foreach (var city in jobSearch.Cities)
            {
                for (int i = 1; i <= pages; i++)
                {
                    locationsAndURLs.Add(new LocationAndURL { URL = templateURL.ToString() + city.ZipCode.ToString() + "&page=" +i, Location = city.CityName });
                }
                
            }

            return locationsAndURLs;
        }


        public static IEnumerable<XElement> Results(string URL)
        {
            XElement monsterFeed;
            try
            {
                WebResponse webResponse = WebRequest.Create(URL).GetResponse();
                StreamReader responseStream = new StreamReader(webResponse.GetResponseStream());
                monsterFeed = XElement.Parse(RemoveInvalidXmlChars(responseStream.ReadToEnd()));
            }

            //If the response fails
            catch(WebException exception)
            {
                return new List<XElement>();
            }
            
            return monsterFeed.Elements().First().Elements().Where(element => element.Name == "item");
        }

        public static AdType parseResult(XElement item, string Location)
        {
            
            if (item.Elements().Where(element => element.Name == "title").FirstOrDefault().Value.Contains("No job"))
            {
                return null;
            }
            
            AdType ad;
            try
            { 
                ad = new AdType
                {
                Title = item.Elements().Where(element => element.Name == "title").FirstOrDefault().Value,
                Network = "Monster",
                Description = item.Elements().Where(element => element.Name == "description").FirstOrDefault().Value,
                URL = item.Elements().Where(element => element.Name == "link").FirstOrDefault().Value,
                Location = Location,
                Date = item.Elements().Where(element => element.Name == "pubDate").FirstOrDefault().Value
                };
            }

            catch(InvalidOperationException exception)
            {
                throw exception;
            }

            return ad;
        }

        private static string RemoveInvalidXmlChars(string text)
        {
            if (text == null || text.Length == 0)
            {
                return text;
            }

            StringBuilder result = new StringBuilder();
            for (int i = 0; i < text.Length; i++)
            {
                if (XmlConvert.IsXmlChar(text[i]))
                {
                    result?.Append(text[i]);
                }
            }

            return result.ToString();
        }
    }
    
}