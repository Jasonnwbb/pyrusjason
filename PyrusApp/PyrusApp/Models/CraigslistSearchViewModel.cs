﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace PyrusApp.Models
{
    public class CraigslistSearchViewModel
    {
        public MultiSelectList Cities { get; set; }
        public MultiSelectList Categories { get; set; }
        public CraigslistSearch Search { get; set; }
    }
}