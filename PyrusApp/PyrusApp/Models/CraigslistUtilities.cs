﻿using HtmlAgilityPack;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Web;
using System.Web.Mvc;
using System.Xml;
using System.Xml.Linq;

namespace PyrusApp.Models
{
    public static class CraigslistUtilities
    {

        public static LinkedList<CraigslistAd> parseFeed(string[] urlCityCategoryTuple, LinkedList<CraigslistAd> ads)
        {
            int initialWebResponseLength = urlCityCategoryTuple[0].IndexOf("&format=rss");
            //WebResponse initialWebResponse = WebRequest.Create(urlCityCategoryTuple[0].Substring(0, initialWebResponseLength)).GetResponse();

            var html = new HtmlDocument();
            int numRequests = 0;
            int totalCount = 0;
            try {
                html.LoadHtml(new WebClient().DownloadString(urlCityCategoryTuple[0].Substring(0, initialWebResponseLength)));
                if (0 < html.DocumentNode.Descendants().Where(n => n.GetAttributeValue("class", "").Equals("totalcount")).Count())
                {
                    var totalCountTag = html.DocumentNode.Descendants().Where(n => n.GetAttributeValue("class", "").Equals("totalcount")).First();
                    totalCount = Convert.ToInt32(totalCountTag.InnerHtml);
                }

                numRequests = totalCount / 25;
                if (totalCount % 25 != 0)
                {
                    numRequests++;
                }
            }
            catch(Exception exception)
            {
                //If the http request is refused, we'll skip that (city,category) pair.
            }
            //20 requests limits the number of results to 200 per category/city

            int requestsLimit = 30;
            if (requestsLimit < numRequests)
            {
                numRequests = requestsLimit;
            }

            for(int i = 0; i < numRequests; i++)
            {
                var webRequest = HttpWebRequest.Create(urlCityCategoryTuple[0] + "&s=" + Convert.ToString(25 * i));
                StreamReader responseStream;
                try
                {
                    responseStream = new StreamReader(webRequest.GetResponse().GetResponseStream());
                }
                catch (WebException exception)
                {
                    return ads;
                }

                //Every now and then the XElement.Parse throws an exception because the craigslist rss
                //feed loaded from a url contains an invalid xml character.  In that case we remove the
                //invalid characters and continue processing
                XElement rssFeed;
                try
                {
                    rssFeed = XElement.Parse(responseStream.ReadToEnd());
                }
                catch (XmlException exception)
                {
                    rssFeed = XElement.Parse(RemoveInvalidXmlChars(responseStream.ReadToEnd()));
                }
                catch (IOException exception)
                {
                    continue;
                }



                Queue<XElement> items = new Queue<XElement>(rssFeed.Elements());

                //dequeue header information before craigslist posts
                items.Dequeue();

                int numAds = items.Count;

                System.Threading.Tasks.Parallel.For(0, numAds, j =>
                {
                    XElement item = items.Dequeue();
                    var ad = processItem(item, urlCityCategoryTuple);
                    if(ad != null)
                    {
                        lock (ads)
                        {

                            ads.AddLast(ad);
                        }
                    }

                });
            };

            return ads;
        }

        private static CraigslistAd processItem(XElement item, string[] urlCityCategoryTuple)
        {
            CraigslistAd ad = new CraigslistAd();
            try { 
                //Get Title of Ad
                ad.Title =
                    item.Elements().Where(tag => tag.ToString().Contains("title")).FirstOrDefault().Value;

                //Extract Asking Price from Title if it exists and clean up the title
                if (ad.Title.Contains("&#x0024;"))
                {
                    string temp = ad.Title.Substring(ad.Title.IndexOf("&#x0024;") + 8);
                    ad.AskingPrice = Int32.Parse(temp);
                    //Clean up the Title
                    ad.Title = ad.Title.Remove(ad.Title.IndexOf("&#x0024;"));

                }
                //Trim the Title
                ad.Title = ad.Title.Trim();

                //Get URL of Ad
                ad.URL =
                    item.Elements().Where(tag => tag.ToString().Contains("dc:source")).First().Value.Trim();

                //Get PartialDescription of ad
                ad.PartialDescription =
                    item.Elements().Where(tag => tag.ToString().Contains("description")).First().Value.Trim();

                //Get TimeStamp of ad
                ad.TimeStamp =
                    item.Elements().Where(tag => tag.ToString().Contains("dc:date")).First().Value.Trim();

                //Get ImageURL of ad if it exists
                if (item.ToString().Contains("image/jpeg"))
                {
                    string tempImageURL =
                    item.Elements().Where(tag => tag.ToString().Contains("image/jpeg")).First().ToString();

                    //Extract SmallImageURL
                    tempImageURL = tempImageURL.Substring(tempImageURL.IndexOf('"') + 1);
                    ad.SmallImageURL = tempImageURL.Remove(tempImageURL.IndexOf('"'));

                    //Get BigImageURL and ThumbnailURL and MediumImageURL from ImageURL
                    ad.BigImageURL = tempImageURL.Remove(tempImageURL.IndexOf("300x300.jpg"));
                    ad.BigImageURL += "1200x900.jpg";

                    ad.ThumbnailImageURL = tempImageURL.Remove(tempImageURL.IndexOf("300x300.jpg"));
                    ad.ThumbnailImageURL += "50x50c.jpg";

                    ad.MediumImageURL = tempImageURL.Remove(tempImageURL.IndexOf("300x300.jpg"));
                    ad.MediumImageURL += "600x450.jpg";
                }

                //Get the ad City
                ad.City = urlCityCategoryTuple[1];

                //Get the ad Category
                ad.Category = urlCityCategoryTuple[2];
            }
            catch(Exception exception)
            {
                return null;
            }

            return ad;
        }

        //Modified version of umei's code. 
        //http://stackoverflow.com/questions/8331119/escape-invalid-xml-characters-in-c-sharp

        private static string RemoveInvalidXmlChars(string text)
        {
            if (text == null || text.Length == 0)
            {
                return text;
            }

            StringBuilder result = new StringBuilder();
            for (int i = 0; i < text.Length; i++)
            {
                if (XmlConvert.IsXmlChar(text[i]))
                {
                    result?.Append(text[i]);
                }
            }

            return result.ToString();
        }

    }
}