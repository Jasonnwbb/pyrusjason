namespace PyrusApp.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class Category
    {
        public int ID { get; set; }

        [Required]
        [StringLength(255)]
        public string CategoryName { get; set; }

        [Required]
        [StringLength(255)]
        public string Extension { get; set; }
    }
}
