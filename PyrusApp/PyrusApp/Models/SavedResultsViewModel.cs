﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PyrusApp.Models
{
    public class SavedResultsViewModel
    {

       public LinkedList<CraigslistAd> SavedAds { get; set; }
       public Dictionary<string, string> AdNotes { get; set; }
    }
}