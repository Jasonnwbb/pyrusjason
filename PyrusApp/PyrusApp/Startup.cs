﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(PyrusApp.Startup))]
namespace PyrusApp
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
