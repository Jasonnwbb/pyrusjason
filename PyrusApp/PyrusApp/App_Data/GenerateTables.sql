﻿--Create Cities Table
CREATE TABLE Cities (
    ID int NOT NULL PRIMARY KEY Identity,
    CityName varchar(255) NOT NULL,
	StateName varchar(255) NOT NULL,
    Extension varchar(255) NOT NULL,
	ZipCode int NOT NULL
);

--Create Craigslist Categories Table
CREATE TABLE Categories (
    ID int NOT NULL PRIMARY KEY Identity,
    CategoryName varchar(255) NOT NULL,
    Extension varchar(255) NOT NULL,
);
