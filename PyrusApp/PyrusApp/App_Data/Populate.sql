﻿--Populate Tables Created in UpScript

--Populate Cities Table
INSERT INTO Cities(CityName, StateName, Extension, ZipCode)
VALUES('Delaware', 'Delaware', 'delaware', 19801); 

INSERT INTO Cities(CityName, StateName, Extension, ZipCode)
VALUES('Altoona-Johnstown', 'Pennsylvania', 'altoona', 16601); 

INSERT INTO Cities(CityName, StateName, Extension, ZipCode)
VALUES('Cumberland Valle', 'Pennsylvania', 'chambersburg', 17201); 

INSERT INTO Cities(CityName, StateName, Extension, ZipCode)
VALUES('Erie', 'Pennsylvania', 'erie', 16501); 

INSERT INTO Cities(CityName, StateName, Extension, ZipCode)
VALUES('Harrisburg', 'Pennsylvania', 'harrisburg', 17025); 

INSERT INTO Cities(CityName, StateName, Extension, ZipCode)
VALUES('Lancaster', 'Pennsylvania', 'lancaster', 17601); 

INSERT INTO Cities(CityName, StateName, Extension, ZipCode)
VALUES('Lehigh Valley', 'Pennsylvania', 'allentown', 18002); 

INSERT INTO Cities(CityName, StateName, Extension, ZipCode)
VALUES('Meadville', 'Pennsylvania', 'meadville', 16335); 

INSERT INTO Cities(CityName, StateName, Extension, ZipCode)
VALUES('Philadelphia', 'Pennsylvania', 'philadelphia', 19019); 

INSERT INTO Cities(CityName, StateName, Extension, ZipCode)
VALUES('Pittsburgh', 'Pennsylvania', 'pittsburgh', 15106); 

INSERT INTO Cities(CityName, StateName, Extension, ZipCode)
VALUES('Poconos', 'Pennsylvania', 'poconos', 18344); 

INSERT INTO Cities(CityName, StateName, Extension, ZipCode)
VALUES('Reading', 'Pennsylvania', 'reading', 19601); 

INSERT INTO Cities(CityName, StateName, Extension, ZipCode)
VALUES('Scranton / Wilkes-Barre', 'Pennsylvania', 'scranton', 18503); 

INSERT INTO Cities(CityName, StateName, Extension, ZipCode)
VALUES('State College (Pennstate)', 'Pennsylvania', 'pennstate', 16801); 

INSERT INTO Cities(CityName, StateName, Extension, ZipCode)
VALUES('Twin Tiers NY/PA', 'Pennsylvania', 'twintiers', 16925); 

INSERT INTO Cities(CityName, StateName, Extension, ZipCode)
VALUES('Williamsport', 'Pennsylvania', 'williamsport', 17701); 

INSERT INTO Cities(CityName, StateName, Extension, ZipCode)
VALUES('York', 'Pennsylvania', 'york', 17401); 

INSERT INTO Cities(CityName, StateName, Extension, ZipCode)
VALUES('Central New Jersey', 'New Jersey', 'cnj', 08527); 

INSERT INTO Cities(CityName, StateName, Extension, ZipCode)
VALUES('Jersey Shore', 'New Jersey', 'jerseyshore', 08234); 

INSERT INTO Cities(CityName, StateName, Extension, ZipCode)
VALUES('North Jersey', 'New Jersey', 'newjersey', 07960); 

INSERT INTO Cities(CityName, StateName, Extension, ZipCode)
VALUES('South Jersey', 'New Jersey', 'southjersey', 08102); 

INSERT INTO Cities(CityName, StateName, Extension, ZipCode)
VALUES('Albany', 'Georgia', 'albanyga', 31701); 

INSERT INTO Cities(CityName, StateName, Extension, ZipCode)
VALUES('Athens', 'Georgia', 'athensga', 30601); 

INSERT INTO Cities(CityName, StateName, Extension, ZipCode)
VALUES('Atlanta', 'Georgia', 'atlanta', 30303); 

INSERT INTO Cities(CityName, StateName, Extension, ZipCode)
VALUES('Augusta', 'Georgia', 'augusta', 30901); 

INSERT INTO Cities(CityName, StateName, Extension, ZipCode)
VALUES('Brunswick', 'Georgia', 'brunswick', 31520); 

INSERT INTO Cities(CityName, StateName, Extension, ZipCode)
VALUES('Columbus', 'Georgia', 'columbusga', 31829); 

INSERT INTO Cities(CityName, StateName, Extension, ZipCode)
VALUES('Macon / Warner Robins', 'Georgia', 'macon', 31201); 

INSERT INTO Cities(CityName, StateName, Extension, ZipCode)
VALUES('Northwest GA', 'Georgia', 'nwga', 30736); 

INSERT INTO Cities(CityName, StateName, Extension, ZipCode)
VALUES('Savannah / Hinesville', 'Georgia', 'savannah', 31302); 

INSERT INTO Cities(CityName, StateName, Extension, ZipCode)
VALUES('Statesboro', 'Georgia', 'statesboro', 30458); 

INSERT INTO Cities(CityName, StateName, Extension, ZipCode)
VALUES('Valdosta', 'Georgia', 'valdosta', 31601); 

INSERT INTO Cities(CityName, StateName, Extension, ZipCode)
VALUES('Eastern CT (New London)', 'Connecticut', 'newlondon', 06320); 

INSERT INTO Cities(CityName, StateName, Extension, ZipCode)
VALUES('Hartford', 'Connecticut', 'hartford', 16101); 

INSERT INTO Cities(CityName, StateName, Extension, ZipCode)
VALUES('New Haven', 'Connecticut', 'newhaven', 06501); 

INSERT INTO Cities(CityName, StateName, Extension, ZipCode)
VALUES('Northwest CT', 'Connecticut', 'nwct', 06791); 

INSERT INTO Cities(CityName, StateName, Extension, ZipCode)
VALUES('Boston', 'Massachusetts', 'boston', 01841); 

INSERT INTO Cities(CityName, StateName, Extension, ZipCode)
VALUES('Cape Cod / Islands', 'Massachusetts', 'capecod', 02630); 

INSERT INTO Cities(CityName, StateName, Extension, ZipCode)
VALUES('South Coast (Southern Bristol)', 'Massachusetts', 'southcoast', 02702); 

INSERT INTO Cities(CityName, StateName, Extension, ZipCode)
VALUES('Western Massachusetts', 'Massachusetts', 'westernmass', 01020); 

INSERT INTO Cities(CityName, StateName, Extension, ZipCode)
VALUES('Worcester / Central MA', 'Massachusetts', 'worcester', 01601); 

INSERT INTO Cities(CityName, StateName, Extension, ZipCode)
VALUES('Annapolis', 'Maryland', 'annapolis', 21401); 

INSERT INTO Cities(CityName, StateName, Extension, ZipCode)
VALUES('Baltimore', 'Maryland', 'baltimore', 21201); 

INSERT INTO Cities(CityName, StateName, Extension, ZipCode)
VALUES('Cumberland Valley', 'Maryland', 'chambersburg', 21502); 

INSERT INTO Cities(CityName, StateName, Extension, ZipCode)
VALUES('Eastern Shore', 'Maryland', 'easternshore', 21842); 

INSERT INTO Cities(CityName, StateName, Extension, ZipCode)
VALUES('Frederick', 'Maryland', 'frederick', 21701); 

INSERT INTO Cities(CityName, StateName, Extension, ZipCode)
VALUES('Southern Maryland', 'Maryland', 'smd', 20601); 

INSERT INTO Cities(CityName, StateName, Extension, ZipCode)
VALUES('Western Maryland', 'Maryland', 'westmd', 21740); 

INSERT INTO Cities(CityName, StateName, Extension, ZipCode)
VALUES('Charleston', 'South Carolina', 'charleston', 29401); 

INSERT INTO Cities(CityName, StateName, Extension, ZipCode)
VALUES('Columbia', 'South Carolina', 'columbia', 29201); 

INSERT INTO Cities(CityName, StateName, Extension, ZipCode)
VALUES('Florence', 'South Carolina', 'florencesc', 29501); 

INSERT INTO Cities(CityName, StateName, Extension, ZipCode)
VALUES('Greenville / Upstate', 'South Carolina', 'greenville', 29601); 

INSERT INTO Cities(CityName, StateName, Extension, ZipCode)
VALUES('Hilton Head', 'South Carolina', 'hiltonhead', 29926); 

INSERT INTO Cities(CityName, StateName, Extension, ZipCode)
VALUES('Myrtle Beach', 'South Carolina', 'myrtlebeach', 29572); 

INSERT INTO Cities(CityName, StateName, Extension, ZipCode)
VALUES('New Hampshire', 'New Hampshire', 'nh', 03101); 

INSERT INTO Cities(CityName, StateName, Extension, ZipCode)
VALUES('Charlottesville', 'Virginia', 'charlottesville', 22901); 

INSERT INTO Cities(CityName, StateName, Extension, ZipCode)
VALUES('Danville', 'Virginia', 'danville', 24540); 

INSERT INTO Cities(CityName, StateName, Extension, ZipCode)
VALUES('Eastern Shore', 'Virginia', 'easternshore', 23336); 

INSERT INTO Cities(CityName, StateName, Extension, ZipCode)
VALUES('Fredericksburg', 'Virginia', 'fredericksburg', 22401); 

INSERT INTO Cities(CityName, StateName, Extension, ZipCode)
VALUES('Hampton Roads', 'Virginia', 'norfolk', 23502); 

INSERT INTO Cities(CityName, StateName, Extension, ZipCode)
VALUES('Harrisonburg', 'Virginia', 'harrisonburg', 22801); 

INSERT INTO Cities(CityName, StateName, Extension, ZipCode)
VALUES('Lynchburg', 'Virginia', 'lynchburg', 24501); 

INSERT INTO Cities(CityName, StateName, Extension, ZipCode)
VALUES('New River Valley', 'Virginia', 'blacksburg', 24060); 

INSERT INTO Cities(CityName, StateName, Extension, ZipCode)
VALUES('Richmond', 'Virginia', 'richmond', 23173); 

INSERT INTO Cities(CityName, StateName, Extension, ZipCode)
VALUES('Roanoke', 'Virginia', 'roanoke', 24011); 

INSERT INTO Cities(CityName, StateName, Extension, ZipCode)
VALUES('Southwest VA', 'Virginia', 'swva', 24333); 

INSERT INTO Cities(CityName, StateName, Extension, ZipCode)
VALUES('Winchester', 'Virginia', 'winchester', 22601); 

INSERT INTO Cities(CityName, StateName, Extension, ZipCode)
VALUES('Albany', 'New York', 'albany', 12202); 

INSERT INTO Cities(CityName, StateName, Extension, ZipCode)
VALUES('Binghamton', 'New York', 'binghamton', 13901); 

INSERT INTO Cities(CityName, StateName, Extension, ZipCode)
VALUES('Buffalo', 'New York', 'buffalo', 14201); 

INSERT INTO Cities(CityName, StateName, Extension, ZipCode)
VALUES('Catskills', 'New York', 'catskills', 12414); 

INSERT INTO Cities(CityName, StateName, Extension, ZipCode)
VALUES('Chautauqua', 'New York', 'chautauqua', 14722); 

INSERT INTO Cities(CityName, StateName, Extension, ZipCode)
VALUES('Elmira-Corning', 'New York', 'elmira', 14901); 

INSERT INTO Cities(CityName, StateName, Extension, ZipCode)
VALUES('Finger Lakes', 'New York', 'fingerlakes', 13148); 

INSERT INTO Cities(CityName, StateName, Extension, ZipCode)
VALUES('Glens Falls', 'New York', 'glensfalls', 12801); 

INSERT INTO Cities(CityName, StateName, Extension, ZipCode)
VALUES('Hudson Valley', 'New York', 'hudsonvalley', 12180); 

INSERT INTO Cities(CityName, StateName, Extension, ZipCode)
VALUES('Ithaca', 'New York', 'ithaca', 14850); 

INSERT INTO Cities(CityName, StateName, Extension, ZipCode)
VALUES('Long Island', 'New York', 'longisland', 11549); 

INSERT INTO Cities(CityName, StateName, Extension, ZipCode)
VALUES('New York City', 'New York', 'newyork', 10001); 

INSERT INTO Cities(CityName, StateName, Extension, ZipCode)
VALUES('Oneonta', 'New York', 'oneonta', 13820); 

INSERT INTO Cities(CityName, StateName, Extension, ZipCode)
VALUES('Plattsburgh-Adirondacks', 'New York', 'plattsburgh', 12901); 

INSERT INTO Cities(CityName, StateName, Extension, ZipCode)
VALUES('Potsdam-Canton-Massena', 'New York', 'potsdam', 13676); 

INSERT INTO Cities(CityName, StateName, Extension, ZipCode)
VALUES('Rochester', 'New York', 'rochester', 14602); 

INSERT INTO Cities(CityName, StateName, Extension, ZipCode)
VALUES('Syracuse', 'New York', 'syracuse', 13201); 

INSERT INTO Cities(CityName, StateName, Extension, ZipCode)
VALUES('Twin Tiers NY/PA', 'New York', 'twintiers', 16925); 

INSERT INTO Cities(CityName, StateName, Extension, ZipCode)
VALUES('Utica-Rome-Oneida', 'New York', 'utica', 13501); 

INSERT INTO Cities(CityName, StateName, Extension, ZipCode)
VALUES('Watertown', 'New York', 'watertown', 13601); 

INSERT INTO Cities(CityName, StateName, Extension, ZipCode)
VALUES('Asheville', 'North Carolina', 'asheville', 28715); 

INSERT INTO Cities(CityName, StateName, Extension, ZipCode)
VALUES('Boone', 'North Carolina', 'boone', 28607); 

INSERT INTO Cities(CityName, StateName, Extension, ZipCode)
VALUES('Charlotte', 'North Carolina', 'charlotte', 28105); 

INSERT INTO Cities(CityName, StateName, Extension, ZipCode)
VALUES('Eastern NC', 'North Carolina', 'eastnc', 27834); 

INSERT INTO Cities(CityName, StateName, Extension, ZipCode)
VALUES('Fayetteville', 'North Carolina', 'fayetteville', 28301); 

INSERT INTO Cities(CityName, StateName, Extension, ZipCode)
VALUES('Greensboro', 'North Carolina', 'greensboro', 27214); 

INSERT INTO Cities(CityName, StateName, Extension, ZipCode)
VALUES('Hickory / Lenoir', 'North Carolina', 'hickory', 28601); 

INSERT INTO Cities(CityName, StateName, Extension, ZipCode)
VALUES('Jacksonville', 'North Carolina', 'onslow', 28540); 

INSERT INTO Cities(CityName, StateName, Extension, ZipCode)
VALUES('Outer Banks', 'North Carolina', 'outerbanks', 27915); 

INSERT INTO Cities(CityName, StateName, Extension, ZipCode)
VALUES('Raleigh / Durham / CH', 'North Carolina', 'raleigh', 27513); 

INSERT INTO Cities(CityName, StateName, Extension, ZipCode)
VALUES('Wilmington', 'North Carolina', 'wilmington', 28401); 

INSERT INTO Cities(CityName, StateName, Extension, ZipCode)
VALUES('Winston-Salem', 'North Carolina', 'winstonsalem', 27023); 

INSERT INTO Cities(CityName, StateName, Extension, ZipCode)
VALUES('Providence', 'Rhode Island', 'providence', 02903); 

INSERT INTO Cities(CityName, StateName, Extension, ZipCode)
VALUES('Vermont', 'Vermont', 'vermont', 05401); 

INSERT INTO Cities(CityName, StateName, Extension, ZipCode)
VALUES('Bowling green', 'Kentucky', 'bgky', 42101); 

INSERT INTO Cities(CityName, StateName, Extension, ZipCode)
VALUES('Cincinnati, oh', 'Kentucky', 'cincinnati', 41073); 

INSERT INTO Cities(CityName, StateName, Extension, ZipCode)
VALUES('Eastern kentucky', 'Kentucky', 'eastky', 40475); 

INSERT INTO Cities(CityName, StateName, Extension, ZipCode)
VALUES('Huntington-ashland', 'Kentucky', 'huntington', 41101); 

INSERT INTO Cities(CityName, StateName, Extension, ZipCode)
VALUES('Lexington', 'Kentucky', 'lexington', 40502); 

INSERT INTO Cities(CityName, StateName, Extension, ZipCode)
VALUES('Louisville', 'Kentucky', 'louisville', 40018); 

INSERT INTO Cities(CityName, StateName, Extension, ZipCode)
VALUES('Owensboro', 'Kentucky', 'owensboro', 42301); 

INSERT INTO Cities(CityName, StateName, Extension, ZipCode)
VALUES('Western ky', 'Kentucky', 'westky', 42101); 

INSERT INTO Cities(CityName, StateName, Extension, ZipCode)
VALUES('Chattanooga', 'Tennessee', 'chattanooga', 37341); 

INSERT INTO Cities(CityName, StateName, Extension, ZipCode)
VALUES('Clarksville', 'Tennessee', 'clarksville', 37040); 

INSERT INTO Cities(CityName, StateName, Extension, ZipCode)
VALUES('Cookeville', 'Tennessee', 'cookeville', 38501); 

INSERT INTO Cities(CityName, StateName, Extension, ZipCode)
VALUES('Jackson', 'Tennessee', 'jacksontn', 38301); 

INSERT INTO Cities(CityName, StateName, Extension, ZipCode)
VALUES('Knoxville', 'Tennessee', 'knoxville', 37901); 

INSERT INTO Cities(CityName, StateName, Extension, ZipCode)
VALUES('Memphis', 'Tennessee', 'memphis', 37501); 

INSERT INTO Cities(CityName, StateName, Extension, ZipCode)
VALUES('Nashville', 'Tennessee', 'nashville', 37011); 

INSERT INTO Cities(CityName, StateName, Extension, ZipCode)
VALUES('Tri-cities', 'Tennessee', 'tricities', 37601); 

INSERT INTO Cities(CityName, StateName, Extension, ZipCode)
VALUES('Akron / canton', 'Ohio', 'akroncanton', 44223); 

INSERT INTO Cities(CityName, StateName, Extension, ZipCode)
VALUES('Ashtabula', 'Ohio', 'ashtabula', 44004); 

INSERT INTO Cities(CityName, StateName, Extension, ZipCode)
VALUES('Athens', 'Ohio', 'athensohio', 45701); 

INSERT INTO Cities(CityName, StateName, Extension, ZipCode)
VALUES('Chillicothe', 'Ohio', 'chillicothe', 45601); 

INSERT INTO Cities(CityName, StateName, Extension, ZipCode)
VALUES('Cincinnati', 'Ohio', 'cincinnati', 41073); 

INSERT INTO Cities(CityName, StateName, Extension, ZipCode)
VALUES('Cleveland', 'Ohio', 'cleveland', 44101); 

INSERT INTO Cities(CityName, StateName, Extension, ZipCode)
VALUES('Columbus', 'Ohio', 'columbus', 43004); 

INSERT INTO Cities(CityName, StateName, Extension, ZipCode)
VALUES('Dayton / springfield', 'Ohio', 'dayton', 45377); 

INSERT INTO Cities(CityName, StateName, Extension, ZipCode)
VALUES('Huntington and ashland', 'Ohio', 'huntington', 44805); 

INSERT INTO Cities(CityName, StateName, Extension, ZipCode)
VALUES('Lima / findlay', 'Ohio', 'limaohio', 45801); 

INSERT INTO Cities(CityName, StateName, Extension, ZipCode)
VALUES('Mansfield', 'Ohio', 'mansfield', 44901); 

INSERT INTO Cities(CityName, StateName, Extension, ZipCode)
VALUES('Northern panhandle', 'Ohio', 'wheeling', 26003); 

INSERT INTO Cities(CityName, StateName, Extension, ZipCode)
VALUES('Parkersburg and marietta', 'Ohio', 'parkersburg', 26101); 

INSERT INTO Cities(CityName, StateName, Extension, ZipCode)
VALUES('Sandusky', 'Ohio', 'sandusky', 44870); 

INSERT INTO Cities(CityName, StateName, Extension, ZipCode)
VALUES('Toledo', 'Ohio', 'toledo', 43460); 

INSERT INTO Cities(CityName, StateName, Extension, ZipCode)
VALUES('Tuscarawas co', 'Ohio', 'tuscarawas', 44629); 

INSERT INTO Cities(CityName, StateName, Extension, ZipCode)
VALUES('Youngstown', 'Ohio', 'youngstown', 44405); 

INSERT INTO Cities(CityName, StateName, Extension, ZipCode)
VALUES('Zanesville / cambridge', 'Ohio', 'zanesville', 43701); 

INSERT INTO Cities(CityName, StateName, Extension, ZipCode)
VALUES('Batonrouge', 'Louisiana', 'baton rouge', 70801); 

INSERT INTO Cities(CityName, StateName, Extension, ZipCode)
VALUES('Central louisiana', 'Louisiana', 'enla', 71301); 

INSERT INTO Cities(CityName, StateName, Extension, ZipCode)
VALUES('Houma', 'Louisiana', 'houma', 70360); 

INSERT INTO Cities(CityName, StateName, Extension, ZipCode)
VALUES('Lafayette', 'Louisiana', 'lafayette', 70501); 

INSERT INTO Cities(CityName, StateName, Extension, ZipCode)
VALUES('Lake charles', 'Louisiana', 'lake charles', 70601); 

INSERT INTO Cities(CityName, StateName, Extension, ZipCode)
VALUES('Monroe', 'Louisiana', 'monroe', 71201); 

INSERT INTO Cities(CityName, StateName, Extension, ZipCode)
VALUES('New orleans', 'Louisiana', 'new orleans', 70032); 

INSERT INTO Cities(CityName, StateName, Extension, ZipCode)
VALUES('Shreveport', 'Louisiana', 'shreveport', 71101); 

INSERT INTO Cities(CityName, StateName, Extension, ZipCode)
VALUES('Bloomington', 'Indiana', 'bloomington', 47401); 

INSERT INTO Cities(CityName, StateName, Extension, ZipCode)
VALUES('Evansville', 'Indiana', 'evansville', 47701); 

INSERT INTO Cities(CityName, StateName, Extension, ZipCode)
VALUES('Fort wayne', 'Indiana', 'fortwayne', 46774); 

INSERT INTO Cities(CityName, StateName, Extension, ZipCode)
VALUES('Indianapolis', 'Indiana', 'indianapolis', 46077); 

INSERT INTO Cities(CityName, StateName, Extension, ZipCode)
VALUES('Kokomo', 'Indiana', 'kokomo', 46901); 

INSERT INTO Cities(CityName, StateName, Extension, ZipCode)
VALUES('Lafayette / west lafayette', 'Indiana', 'tippecanoe', 46570); 

INSERT INTO Cities(CityName, StateName, Extension, ZipCode)
VALUES('Muncie / anderson', 'Indiana', 'muncie', 47302); 

INSERT INTO Cities(CityName, StateName, Extension, ZipCode)
VALUES('Richmond', 'Indiana', 'richmondin', 47374); 

INSERT INTO Cities(CityName, StateName, Extension, ZipCode)
VALUES('South bend / michiana', 'Indiana', 'southbend', 46556); 

INSERT INTO Cities(CityName, StateName, Extension, ZipCode)
VALUES('Terre haute', 'Indiana', 'terrehaute', 47801); 

INSERT INTO Cities(CityName, StateName, Extension, ZipCode)
VALUES('Northwest indiana (subregion of chicago site)', 'Indiana', 'chicago', 46320); 

INSERT INTO Cities(CityName, StateName, Extension, ZipCode)
VALUES('Gulfport / biloxi', 'Mississippi', 'gulfport', 39501); 

INSERT INTO Cities(CityName, StateName, Extension, ZipCode)
VALUES('Hattiesburg', 'Mississippi', 'hattiesburg', 39401); 

INSERT INTO Cities(CityName, StateName, Extension, ZipCode)
VALUES('Jackson', 'Mississippi', 'jackson', 39056); 

INSERT INTO Cities(CityName, StateName, Extension, ZipCode)
VALUES('Memphis, TN', 'Mississippi', 'memphis', 37501); 

INSERT INTO Cities(CityName, StateName, Extension, ZipCode)
VALUES('Meridian', 'Mississippi', 'meridian', 39301); 

INSERT INTO Cities(CityName, StateName, Extension, ZipCode)
VALUES('North mississippi', 'Mississippi', 'northmiss', 38651); 

INSERT INTO Cities(CityName, StateName, Extension, ZipCode)
VALUES('Southwest MS', 'Mississippi', 'natchez', 39120); 

INSERT INTO Cities(CityName, StateName, Extension, ZipCode)
VALUES('Bloomington-normal', 'Illinois', 'bn', 61701); 

INSERT INTO Cities(CityName, StateName, Extension, ZipCode)
VALUES('Champaign urbana', 'Illinois', 'chambana', 61801); 

INSERT INTO Cities(CityName, StateName, Extension, ZipCode)
VALUES('Chicago', 'Illinois', 'chicago', 60007); 

INSERT INTO Cities(CityName, StateName, Extension, ZipCode)
VALUES('Decatur', 'Illinois', 'decatur', 62501); 

INSERT INTO Cities(CityName, StateName, Extension, ZipCode)
VALUES('La salle co', 'Illinois', 'lasalle', 61301); 

INSERT INTO Cities(CityName, StateName, Extension, ZipCode)
VALUES('Mattoon and charleston', 'Illinois', 'mattoon', 61938); 

INSERT INTO Cities(CityName, StateName, Extension, ZipCode)
VALUES('Peoria', 'Illinois', 'peoria', 61525); 

INSERT INTO Cities(CityName, StateName, Extension, ZipCode)
VALUES('Quad cities, IA/IL', 'Illinois', 'quadcities', 61240); 

INSERT INTO Cities(CityName, StateName, Extension, ZipCode)
VALUES('Rockford', 'Illinois', 'rockford', 61016); 

INSERT INTO Cities(CityName, StateName, Extension, ZipCode)
VALUES('Southern illinois', 'Illinois', 'carbondale', 62901); 

INSERT INTO Cities(CityName, StateName, Extension, ZipCode)
VALUES('Springfield', 'Illinois', 'springfieldil', 62629); 

INSERT INTO Cities(CityName, StateName, Extension, ZipCode)
VALUES('St louis, MO', 'Illinois', 'stlouis', 62071); 

INSERT INTO Cities(CityName, StateName, Extension, ZipCode)
VALUES('Western IL', 'Illinois', 'quincy', 62301); 

INSERT INTO Cities(CityName, StateName, Extension, ZipCode)
VALUES('Auburn', 'Alabama', 'auburn', 36801); 

INSERT INTO Cities(CityName, StateName, Extension, ZipCode)
VALUES('Birmingham', 'Alabama', 'bham', 35005); 

INSERT INTO Cities(CityName, StateName, Extension, ZipCode)
VALUES('Columbus, GA', 'Alabama', 'columbusga', 31829); 

INSERT INTO Cities(CityName, StateName, Extension, ZipCode)
VALUES('Dothan', 'Alabama', 'dothan', 36301); 

INSERT INTO Cities(CityName, StateName, Extension, ZipCode)
VALUES('Florence / muscle shoals', 'Alabama', 'shoals', 35646); 

INSERT INTO Cities(CityName, StateName, Extension, ZipCode)
VALUES('Gadsden and anniston', 'Alabama', 'gadsden', 35901); 

INSERT INTO Cities(CityName, StateName, Extension, ZipCode)
VALUES('Huntsville / decatur', 'Alabama', 'huntsville', 35649); 

INSERT INTO Cities(CityName, StateName, Extension, ZipCode)
VALUES('Mobile', 'Alabama', 'mobile', 36525); 

INSERT INTO Cities(CityName, StateName, Extension, ZipCode)
VALUES('Montgomery', 'Alabama', 'montgomery', 36043); 

INSERT INTO Cities(CityName, StateName, Extension, ZipCode)
VALUES('Tuscaloosa', 'Alabama', 'tuscaloosa', 35401); 

INSERT INTO Cities(CityName, StateName, Extension, ZipCode)
VALUES('Maine', 'Maine', 'maine', 04019); 

INSERT INTO Cities(CityName, StateName, Extension, ZipCode)
VALUES('Columbia / jeff city', 'Missouri', 'columbiamo', 65201); 

INSERT INTO Cities(CityName, StateName, Extension, ZipCode)
VALUES('Joplin', 'Missouri', 'joplin', 64801); 

INSERT INTO Cities(CityName, StateName, Extension, ZipCode)
VALUES('Kansas city', 'Missouri', 'kansascity', 64030); 

INSERT INTO Cities(CityName, StateName, Extension, ZipCode)
VALUES('Kirksville', 'Missouri', 'kirksville', 63501); 

INSERT INTO Cities(CityName, StateName, Extension, ZipCode)
VALUES('Lake of the ozarks', 'Missouri', 'loz', 65049); 

INSERT INTO Cities(CityName, StateName, Extension, ZipCode)
VALUES('Southeast missouri', 'Missouri', 'semo', 63701); 

INSERT INTO Cities(CityName, StateName, Extension, ZipCode)
VALUES('Springfield', 'Missouri', 'springfield', 65619); 

INSERT INTO Cities(CityName, StateName, Extension, ZipCode)
VALUES('St joseph', 'Missouri', 'stjoseph', 64501); 

INSERT INTO Cities(CityName, StateName, Extension, ZipCode)
VALUES('St louis', 'Missouri', 'stlouis', 63101); 

INSERT INTO Cities(CityName, StateName, Extension, ZipCode)
VALUES('Fayetteville', 'Arkansas', 'fayar', 72701); 

INSERT INTO Cities(CityName, StateName, Extension, ZipCode)
VALUES('Fort smith', 'Arkansas', 'fortsmith', 72901); 

INSERT INTO Cities(CityName, StateName, Extension, ZipCode)
VALUES('Jonesboro', 'Arkansas', 'jonesboro', 72401); 

INSERT INTO Cities(CityName, StateName, Extension, ZipCode)
VALUES('Little rock', 'Arkansas', 'littlerock', 72002); 

INSERT INTO Cities(CityName, StateName, Extension, ZipCode)
VALUES('Memphis, TN', 'Arkansas', 'memphis', 37501); 

INSERT INTO Cities(CityName, StateName, Extension, ZipCode)
VALUES('Texarkana', 'Arkansas', 'texarkana', 71854); 

INSERT INTO Cities(CityName, StateName, Extension, ZipCode)
VALUES('Ann arbor', 'Michigan', 'annarbor', 48103); 

INSERT INTO Cities(CityName, StateName, Extension, ZipCode)
VALUES('Battle creek', 'Michigan', 'battlecreek', 49014); 

INSERT INTO Cities(CityName, StateName, Extension, ZipCode)
VALUES('Central michigan', 'Michigan', 'centralmich', 48858); 

INSERT INTO Cities(CityName, StateName, Extension, ZipCode)
VALUES('Detroit metro', 'Michigan', 'detroit', 48127); 

INSERT INTO Cities(CityName, StateName, Extension, ZipCode)
VALUES('Flint', 'Michigan', 'flint', 48501); 

INSERT INTO Cities(CityName, StateName, Extension, ZipCode)
VALUES('Grand rapids', 'Michigan', 'grandrapids', 49501); 

INSERT INTO Cities(CityName, StateName, Extension, ZipCode)
VALUES('Holland', 'Michigan', 'holland', 49422); 

INSERT INTO Cities(CityName, StateName, Extension, ZipCode)
VALUES('Jackson', 'Michigan', 'jxn', 49202); 

INSERT INTO Cities(CityName, StateName, Extension, ZipCode)
VALUES('Kalamazoo', 'Michigan', 'kalamazoo', 49001); 

INSERT INTO Cities(CityName, StateName, Extension, ZipCode)
VALUES('Lansing', 'Michigan', 'lansing', 48864); 

INSERT INTO Cities(CityName, StateName, Extension, ZipCode)
VALUES('Monroe', 'Michigan', 'monroemi', 48161); 

INSERT INTO Cities(CityName, StateName, Extension, ZipCode)
VALUES('Muskegon', 'Michigan', 'muskegon', 49440); 

INSERT INTO Cities(CityName, StateName, Extension, ZipCode)
VALUES('Northern michigan', 'Michigan', 'nmi', 49855); 

INSERT INTO Cities(CityName, StateName, Extension, ZipCode)
VALUES('Port huron', 'Michigan', 'porthuron', 48060); 

INSERT INTO Cities(CityName, StateName, Extension, ZipCode)
VALUES('Saginaw and midland and baycity', 'Michigan', 'saginaw', 48601); 

INSERT INTO Cities(CityName, StateName, Extension, ZipCode)
VALUES('South bend / michiana', 'Michigan', 'southbend', 49117); 

INSERT INTO Cities(CityName, StateName, Extension, ZipCode)
VALUES('Southwest michigan', 'Michigan', 'swmi', 49022); 

INSERT INTO Cities(CityName, StateName, Extension, ZipCode)
VALUES('The thumb', 'Michigan', 'thumb', 48413); 

INSERT INTO Cities(CityName, StateName, Extension, ZipCode)
VALUES('Upper peninsula', 'Michigan', 'up', 49855); 

INSERT INTO Cities(CityName, StateName, Extension, ZipCode)
VALUES('Daytona beach', 'Florida', 'daytona', 32114); 

INSERT INTO Cities(CityName, StateName, Extension, ZipCode)
VALUES('Florida keys', 'Florida', 'keys', 33040); 

INSERT INTO Cities(CityName, StateName, Extension, ZipCode)
VALUES('Ft myers / SW florida', 'Florida', 'fortmyers', 33901); 

INSERT INTO Cities(CityName, StateName, Extension, ZipCode)
VALUES('Gainesville', 'Florida', 'gainesville', 32601); 

INSERT INTO Cities(CityName, StateName, Extension, ZipCode)
VALUES('Heartland florida', 'Florida', 'cfl', 33870); 

INSERT INTO Cities(CityName, StateName, Extension, ZipCode)
VALUES('Jacksonville', 'Florida', 'jacksonville', 32034); 

INSERT INTO Cities(CityName, StateName, Extension, ZipCode)
VALUES('Lakeland', 'Florida', 'lakeland', 33801); 

INSERT INTO Cities(CityName, StateName, Extension, ZipCode)
VALUES('North central FL', 'Florida', 'lakecity', 32025); 

INSERT INTO Cities(CityName, StateName, Extension, ZipCode)
VALUES('Ocala', 'Florida', 'ocala', 34470); 

INSERT INTO Cities(CityName, StateName, Extension, ZipCode)
VALUES('Okaloosa / walton', 'Florida', 'okaloosa', 32547); 

INSERT INTO Cities(CityName, StateName, Extension, ZipCode)
VALUES('Orlando', 'Florida', 'orlando', 32789); 

INSERT INTO Cities(CityName, StateName, Extension, ZipCode)
VALUES('Panama city', 'Florida', 'panamacity', 32401); 

INSERT INTO Cities(CityName, StateName, Extension, ZipCode)
VALUES('Pensacola', 'Florida', 'pensacola', 32501); 

INSERT INTO Cities(CityName, StateName, Extension, ZipCode)
VALUES('Sarasota and bradenton', 'Florida', 'sarasota', 34230); 

INSERT INTO Cities(CityName, StateName, Extension, ZipCode)
VALUES('South florida - includes separate sections for miami/dade, broward, and palm beach counties', 'Florida', 'miami', 33018); 

INSERT INTO Cities(CityName, StateName, Extension, ZipCode)
VALUES('Space coast', 'Florida', 'spacecoast', 32904); 

INSERT INTO Cities(CityName, StateName, Extension, ZipCode)
VALUES('St augustine', 'Florida', 'staugustine', 32080); 

INSERT INTO Cities(CityName, StateName, Extension, ZipCode)
VALUES('Tallahassee', 'Florida', 'tallahassee', 32301); 

INSERT INTO Cities(CityName, StateName, Extension, ZipCode)
VALUES('Tampa bay area', 'Florida', 'tampa', 33601); 

INSERT INTO Cities(CityName, StateName, Extension, ZipCode)
VALUES('Treasure coast', 'Florida', 'treasure', 33706); 

INSERT INTO Cities(CityName, StateName, Extension, ZipCode)
VALUES('Abilene', 'Texas', 'abilene', 79601); 

INSERT INTO Cities(CityName, StateName, Extension, ZipCode)
VALUES('Amarillo', 'Texas', 'amarillo', 79101); 

INSERT INTO Cities(CityName, StateName, Extension, ZipCode)
VALUES('Austin', 'Texas', 'austin', 73301); 

INSERT INTO Cities(CityName, StateName, Extension, ZipCode)
VALUES('Beaumont / port arthur', 'Texas', 'beaumont', 77701); 

INSERT INTO Cities(CityName, StateName, Extension, ZipCode)
VALUES('Brownsville', 'Texas', 'brownsville', 78520); 

INSERT INTO Cities(CityName, StateName, Extension, ZipCode)
VALUES('College station', 'Texas', 'collegestation', 77802); 

INSERT INTO Cities(CityName, StateName, Extension, ZipCode)
VALUES('Corpus christi', 'Texas', 'corpuschristi', 78336); 

INSERT INTO Cities(CityName, StateName, Extension, ZipCode)
VALUES('Dallas / fort worth', 'Texas', 'dallas', 75001); 

INSERT INTO Cities(CityName, StateName, Extension, ZipCode)
VALUES('Deep east texas', 'Texas', 'nacogdoches', 75961); 

INSERT INTO Cities(CityName, StateName, Extension, ZipCode)
VALUES('Del rio / eagle pass', 'Texas', 'delrio', 78840); 

INSERT INTO Cities(CityName, StateName, Extension, ZipCode)
VALUES('El paso', 'Texas', 'elpaso', 79835); 

INSERT INTO Cities(CityName, StateName, Extension, ZipCode)
VALUES('Galveston', 'Texas', 'galveston', 77550); 

INSERT INTO Cities(CityName, StateName, Extension, ZipCode)
VALUES('Houston', 'Texas', 'houston', 77001); 

INSERT INTO Cities(CityName, StateName, Extension, ZipCode)
VALUES('Killeen / temple / ft hood', 'Texas', 'killeen', 76540); 

INSERT INTO Cities(CityName, StateName, Extension, ZipCode)
VALUES('Laredo', 'Texas', 'laredo', 78040); 

INSERT INTO Cities(CityName, StateName, Extension, ZipCode)
VALUES('Lubbock', 'Texas', 'lubbock', 79382); 

INSERT INTO Cities(CityName, StateName, Extension, ZipCode)
VALUES('Mcallen / edinburg', 'Texas', 'mcallen', 78501); 

INSERT INTO Cities(CityName, StateName, Extension, ZipCode)
VALUES('Odessa / midland', 'Texas', 'odessa', 79760); 

INSERT INTO Cities(CityName, StateName, Extension, ZipCode)
VALUES('San angelo', 'Texas', 'sanangelo', 76901); 

INSERT INTO Cities(CityName, StateName, Extension, ZipCode)
VALUES('San antonio', 'Texas', 'sanantonio', 78006); 

INSERT INTO Cities(CityName, StateName, Extension, ZipCode)
VALUES('San marcos', 'Texas', 'sanmarcos', 78656); 

INSERT INTO Cities(CityName, StateName, Extension, ZipCode)
VALUES('Southwest TX', 'Texas', 'bigbend', 79834); 

INSERT INTO Cities(CityName, StateName, Extension, ZipCode)
VALUES('Texarkana', 'Texas', 'texarkana', 75501); 

INSERT INTO Cities(CityName, StateName, Extension, ZipCode)
VALUES('Texoma', 'Texas', 'texoma', 73949); 

INSERT INTO Cities(CityName, StateName, Extension, ZipCode)
VALUES('Tyler / east TX', 'Texas', 'easttexas', 75701); 

INSERT INTO Cities(CityName, StateName, Extension, ZipCode)
VALUES('Victoria', 'Texas', 'victoriatx', 77901); 

INSERT INTO Cities(CityName, StateName, Extension, ZipCode)
VALUES('Waco', 'Texas', 'waco', 76633); 

INSERT INTO Cities(CityName, StateName, Extension, ZipCode)
VALUES('Wichita falls', 'Texas', 'wichitafalls', 76301); 

INSERT INTO Cities(CityName, StateName, Extension, ZipCode)
VALUES('Ames', 'Iowa', 'ames', 50010); 

INSERT INTO Cities(CityName, StateName, Extension, ZipCode)
VALUES('Cedar rapids', 'Iowa', 'cedarrapids', 52227); 

INSERT INTO Cities(CityName, StateName, Extension, ZipCode)
VALUES('Des moines', 'Iowa', 'desmoines', 50047); 

INSERT INTO Cities(CityName, StateName, Extension, ZipCode)
VALUES('Dubuque', 'Iowa', 'dubuque', 52001); 

INSERT INTO Cities(CityName, StateName, Extension, ZipCode)
VALUES('Fort dodge', 'Iowa', 'fortdodge', 50501); 

INSERT INTO Cities(CityName, StateName, Extension, ZipCode)
VALUES('Iowa city', 'Iowa', 'iowacity', 52240); 

INSERT INTO Cities(CityName, StateName, Extension, ZipCode)
VALUES('Mason city', 'Iowa', 'masoncity', 50401); 

INSERT INTO Cities(CityName, StateName, Extension, ZipCode)
VALUES('Omaha / council bluffs', 'Iowa', 'omaha', 51501); 

INSERT INTO Cities(CityName, StateName, Extension, ZipCode)
VALUES('Quad cities, IA/IL', 'Iowa', 'quadcities', 61240); 

INSERT INTO Cities(CityName, StateName, Extension, ZipCode)
VALUES('Sioux city', 'Iowa', 'siouxcity', 51054); 

INSERT INTO Cities(CityName, StateName, Extension, ZipCode)
VALUES('Southeast IA', 'Iowa', 'ottumwa', 52501); 

INSERT INTO Cities(CityName, StateName, Extension, ZipCode)
VALUES('Waterloo / cedar falls', 'Iowa', 'waterloo', 50701); 

INSERT INTO Cities(CityName, StateName, Extension, ZipCode)
VALUES('Appleton-oshkosh-FDL', 'Wisconsin', 'appleton', 54911); 

INSERT INTO Cities(CityName, StateName, Extension, ZipCode)
VALUES('Duluth / superior', 'Wisconsin', 'duluth', 54874); 

INSERT INTO Cities(CityName, StateName, Extension, ZipCode)
VALUES('Eau claire', 'Wisconsin', 'eauclaire', 54701); 

INSERT INTO Cities(CityName, StateName, Extension, ZipCode)
VALUES('Green bay', 'Wisconsin', 'greenbay', 54229); 

INSERT INTO Cities(CityName, StateName, Extension, ZipCode)
VALUES('Janesville', 'Wisconsin', 'janesville', 53542); 

INSERT INTO Cities(CityName, StateName, Extension, ZipCode)
VALUES('Kenosha-racine', 'Wisconsin', 'racine', 53401); 

INSERT INTO Cities(CityName, StateName, Extension, ZipCode)
VALUES('La crosse', 'Wisconsin', 'lacrosse', 54601); 

INSERT INTO Cities(CityName, StateName, Extension, ZipCode)
VALUES('Madison', 'Wisconsin', 'madison', 53558); 

INSERT INTO Cities(CityName, StateName, Extension, ZipCode)
VALUES('Milwaukee', 'Wisconsin', 'milwaukee', 53172); 

INSERT INTO Cities(CityName, StateName, Extension, ZipCode)
VALUES('Northern WI', 'Wisconsin', 'northernwi', 54102); 

INSERT INTO Cities(CityName, StateName, Extension, ZipCode)
VALUES('Sheboygan', 'Wisconsin', 'sheboygan', 53081); 

INSERT INTO Cities(CityName, StateName, Extension, ZipCode)
VALUES('Wausau', 'Wisconsin', 'wausau', 54401); 

INSERT INTO Cities(CityName, StateName, Extension, ZipCode)
VALUES('Bakersfield', 'California', 'bakersfield', 93301); 

INSERT INTO Cities(CityName, StateName, Extension, ZipCode)
VALUES('Chico', 'California', 'chico', 95926); 

INSERT INTO Cities(CityName, StateName, Extension, ZipCode)
VALUES('Fresno / madera', 'California', 'fresno', 93650); 

INSERT INTO Cities(CityName, StateName, Extension, ZipCode)
VALUES('Gold country', 'California', 'goldcountry', 95670); 

INSERT INTO Cities(CityName, StateName, Extension, ZipCode)
VALUES('Hanford-corcoran', 'California', 'hanford', 93230); 

INSERT INTO Cities(CityName, StateName, Extension, ZipCode)
VALUES('Humboldt county', 'California', 'ohio', 95518); 

INSERT INTO Cities(CityName, StateName, Extension, ZipCode)
VALUES('Imperial county', 'California', 'imperial', 92251); 

INSERT INTO Cities(CityName, StateName, Extension, ZipCode)
VALUES('Inland empire - riverside and san bernardino counties', 'California', 'inlandempire', 92501); 

INSERT INTO Cities(CityName, StateName, Extension, ZipCode)
VALUES('Los angeles', 'California', 'losangeles', 90001); 

INSERT INTO Cities(CityName, StateName, Extension, ZipCode)
VALUES('Mendocino county', 'California', 'mendocino', 95460); 

INSERT INTO Cities(CityName, StateName, Extension, ZipCode)
VALUES('Merced', 'California', 'merced', 95340); 

INSERT INTO Cities(CityName, StateName, Extension, ZipCode)
VALUES('Modesto', 'California', 'modesto', 95350); 

INSERT INTO Cities(CityName, StateName, Extension, ZipCode)
VALUES('Monterey bay', 'California', 'monterey', 93940); 

INSERT INTO Cities(CityName, StateName, Extension, ZipCode)
VALUES('Orange county', 'California', 'orangecounty', 92701); 

INSERT INTO Cities(CityName, StateName, Extension, ZipCode)
VALUES('Palm springs', 'California', 'palmsprings', 92240); 

INSERT INTO Cities(CityName, StateName, Extension, ZipCode)
VALUES('Redding', 'California', 'redding', 96001); 

INSERT INTO Cities(CityName, StateName, Extension, ZipCode)
VALUES('Reno / tahoe', 'California', 'reno', 96150); 

INSERT INTO Cities(CityName, StateName, Extension, ZipCode)
VALUES('Sacramento', 'California', 'sacramento', 94203); 

INSERT INTO Cities(CityName, StateName, Extension, ZipCode)
VALUES('San diego', 'California', 'sandiego', 92101); 

INSERT INTO Cities(CityName, StateName, Extension, ZipCode)
VALUES('San luis obispo', 'California', 'slo', 93401); 

INSERT INTO Cities(CityName, StateName, Extension, ZipCode)
VALUES('Santa barbara', 'California', 'santabarbara', 93101); 

INSERT INTO Cities(CityName, StateName, Extension, ZipCode)
VALUES('Santa maria', 'California', 'santamaria', 93454); 

INSERT INTO Cities(CityName, StateName, Extension, ZipCode)
VALUES('SF bay area', 'California', 'sfbay', 94016); 

INSERT INTO Cities(CityName, StateName, Extension, ZipCode)
VALUES('Siskiyou county', 'California', 'siskiyou', 96027); 

INSERT INTO Cities(CityName, StateName, Extension, ZipCode)
VALUES('Stockton', 'California', 'stockton', 95201); 

INSERT INTO Cities(CityName, StateName, Extension, ZipCode)
VALUES('Susanville', 'California', 'susanville', 96127); 

INSERT INTO Cities(CityName, StateName, Extension, ZipCode)
VALUES('Ventura county', 'California', 'ventura', 93001); 

INSERT INTO Cities(CityName, StateName, Extension, ZipCode)
VALUES('Visalia-tulare', 'California', 'visalia', 93277); 

INSERT INTO Cities(CityName, StateName, Extension, ZipCode)
VALUES('Yuba-sutter', 'California', 'yubasutter', 95991); 

INSERT INTO Cities(CityName, StateName, Extension, ZipCode)
VALUES('Bemidji', 'Minnesota', 'bemidji', 56601); 

INSERT INTO Cities(CityName, StateName, Extension, ZipCode)
VALUES('Brainerd', 'Minnesota', 'brainerd', 56401); 

INSERT INTO Cities(CityName, StateName, Extension, ZipCode)
VALUES('Duluth / superior', 'Minnesota', 'duluth', 55802); 

INSERT INTO Cities(CityName, StateName, Extension, ZipCode)
VALUES('Fargo / moorhead', 'Minnesota', 'fargo', 56529); 

INSERT INTO Cities(CityName, StateName, Extension, ZipCode)
VALUES('Mankato', 'Minnesota', 'mankato', 56001); 

INSERT INTO Cities(CityName, StateName, Extension, ZipCode)
VALUES('Minneapolis / stpaul', 'Minnesota', 'minneapolis', 55111); 

INSERT INTO Cities(CityName, StateName, Extension, ZipCode)
VALUES('Rochester', 'Minnesota', 'rmn', 55901); 

INSERT INTO Cities(CityName, StateName, Extension, ZipCode)
VALUES('Southwest MN', 'Minnesota', 'marshall', 56258); 

INSERT INTO Cities(CityName, StateName, Extension, ZipCode)
VALUES('St Cloud', 'Minnesota', 'stcloud', 56301); 

INSERT INTO Cities(CityName, StateName, Extension, ZipCode)
VALUES('Lawrence', 'Kansas', 'lawrence', 66044); 

INSERT INTO Cities(CityName, StateName, Extension, ZipCode)
VALUES('Manhattan', 'Kansas', 'ksu', 66502); 

INSERT INTO Cities(CityName, StateName, Extension, ZipCode)
VALUES('Northwest Kansas', 'Kansas', 'nwks', 67735); 

INSERT INTO Cities(CityName, StateName, Extension, ZipCode)
VALUES('Salina', 'Kansas', 'salina', 67401); 

INSERT INTO Cities(CityName, StateName, Extension, ZipCode)
VALUES('Southeast Kansas', 'Kansas', 'seks', 66720); 

INSERT INTO Cities(CityName, StateName, Extension, ZipCode)
VALUES('Southwest Kansas', 'Kansas', 'swks', 67156); 

INSERT INTO Cities(CityName, StateName, Extension, ZipCode)
VALUES('Topeka', 'Kansas', 'topeka', 66546); 

INSERT INTO Cities(CityName, StateName, Extension, ZipCode)
VALUES('Wichita', 'Kansas', 'wichita', 67202); 

INSERT INTO Cities(CityName, StateName, Extension, ZipCode)
VALUES('Charleston', 'West Virginia', 'charlestonwv', 25301); 

INSERT INTO Cities(CityName, StateName, Extension, ZipCode)
VALUES('Eastern Panhandle (Martinsburg)', 'West Virginia', 'martinsburg', 25401); 

INSERT INTO Cities(CityName, StateName, Extension, ZipCode)
VALUES('Huntington and Ashland', 'West Virginia', 'huntington', 22303); 

INSERT INTO Cities(CityName, StateName, Extension, ZipCode)
VALUES('Morgantown', 'West Virginia', 'morgantown', 26501); 

INSERT INTO Cities(CityName, StateName, Extension, ZipCode)
VALUES('Northern Panhandle (Wheeling)', 'West Virginia', 'wheeling', 26003); 

INSERT INTO Cities(CityName, StateName, Extension, ZipCode)
VALUES('Parkersburg and Marietta', 'West Virginia', 'parkersburg', 26101); 

INSERT INTO Cities(CityName, StateName, Extension, ZipCode)
VALUES('Southern West Virginia', 'West Virginia', 'swv', 25801); 

INSERT INTO Cities(CityName, StateName, Extension, ZipCode)
VALUES('West Virginia (old)', 'West Virginia', 'wv', 25301); 

INSERT INTO Cities(CityName, StateName, Extension, ZipCode)
VALUES('Elko', 'Nevada', 'elko', 89801); 

INSERT INTO Cities(CityName, StateName, Extension, ZipCode)
VALUES('Las Vegas', 'Nevada', 'lasvegas', 88901); 

INSERT INTO Cities(CityName, StateName, Extension, ZipCode)
VALUES('Reno and Tahoe', 'Nevada', 'reno', 89433); 

INSERT INTO Cities(CityName, StateName, Extension, ZipCode)
VALUES('Grand Island', 'Nebraska', 'grandisland', 68801); 

INSERT INTO Cities(CityName, StateName, Extension, ZipCode)
VALUES('Lincoln', 'Nebraska', 'lincoln', 68501); 

INSERT INTO Cities(CityName, StateName, Extension, ZipCode)
VALUES('North Platte', 'Nebraska', 'northplatte', 69101); 

INSERT INTO Cities(CityName, StateName, Extension, ZipCode)
VALUES('Omaha and Council Bluffs', 'Nebraska', 'omaha', 68007); 

INSERT INTO Cities(CityName, StateName, Extension, ZipCode)
VALUES('Scottsbluff and Panhandle', 'Nebraska', 'scottsbluff', 69361); 

INSERT INTO Cities(CityName, StateName, Extension, ZipCode)
VALUES('Boulder', 'Colorado', 'boulder', 80301); 

INSERT INTO Cities(CityName, StateName, Extension, ZipCode)
VALUES('Colorado Springs', 'Colorado', 'cosprings', 80829); 

INSERT INTO Cities(CityName, StateName, Extension, ZipCode)
VALUES('Denver', 'Colorado', 'denver', 80014); 

INSERT INTO Cities(CityName, StateName, Extension, ZipCode)
VALUES('Eastern Colorado', 'Colorado', 'eastco', 80751); 

INSERT INTO Cities(CityName, StateName, Extension, ZipCode)
VALUES('Northern Colorado (Fort Collins)', 'Colorado', 'fortcollins', 80521); 

INSERT INTO Cities(CityName, StateName, Extension, ZipCode)
VALUES('High Rockies', 'Colorado', 'rockies', 80477); 

INSERT INTO Cities(CityName, StateName, Extension, ZipCode)
VALUES('Pueblo', 'Colorado', 'pueblo', 81001); 

INSERT INTO Cities(CityName, StateName, Extension, ZipCode)
VALUES('Western Slope', 'Colorado', 'westslope', 81501); 

INSERT INTO Cities(CityName, StateName, Extension, ZipCode)
VALUES('Bismarck', 'North Dakota', 'bismarck', 58501); 

INSERT INTO Cities(CityName, StateName, Extension, ZipCode)
VALUES('Fargo and Moorhead', 'North Dakota', 'fargo', 58102); 

INSERT INTO Cities(CityName, StateName, Extension, ZipCode)
VALUES('Grand Forks', 'North Dakota', 'grandforks', 58201); 

INSERT INTO Cities(CityName, StateName, Extension, ZipCode)
VALUES('North Dakota', 'North Dakota', 'nd', 58701); 

INSERT INTO Cities(CityName, StateName, Extension, ZipCode)
VALUES('Northeast South Dakota', 'South Dakota', 'nesd', 57401); 

INSERT INTO Cities(CityName, StateName, Extension, ZipCode)
VALUES('Central South Dakota (Pierre)', 'South Dakota', 'csd', 57501); 

INSERT INTO Cities(CityName, StateName, Extension, ZipCode)
VALUES('West South Dakota (Rapid City)', 'South Dakota', 'rapidcity', 57701); 

INSERT INTO Cities(CityName, StateName, Extension, ZipCode)
VALUES('Southeastern South Dakota (Sioux Falls)', 'South Dakota', 'siouxfalls', 57101); 

INSERT INTO Cities(CityName, StateName, Extension, ZipCode)
VALUES('South Dakota', 'South Dakota', 'sd', 57401); 

INSERT INTO Cities(CityName, StateName, Extension, ZipCode)
VALUES('Billings', 'Montana', 'billings', 59101); 

INSERT INTO Cities(CityName, StateName, Extension, ZipCode)
VALUES('Bozeman', 'Montana', 'bozeman', 59715); 

INSERT INTO Cities(CityName, StateName, Extension, ZipCode)
VALUES('Butte', 'Montana', 'butte', 59701); 

INSERT INTO Cities(CityName, StateName, Extension, ZipCode)
VALUES('Eastern Montana', 'Montana', 'montana', 59270); 

INSERT INTO Cities(CityName, StateName, Extension, ZipCode)
VALUES('Great Falls', 'Montana', 'greatfalls', 59401); 

INSERT INTO Cities(CityName, StateName, Extension, ZipCode)
VALUES('Helena', 'Montana', 'helena', 59601); 

INSERT INTO Cities(CityName, StateName, Extension, ZipCode)
VALUES('Kalispell', 'Montana', 'kalispell', 59901); 

INSERT INTO Cities(CityName, StateName, Extension, ZipCode)
VALUES('Missoula', 'Montana', 'missoula', 59801); 

INSERT INTO Cities(CityName, StateName, Extension, ZipCode)
VALUES('Bellingham', 'Washington', 'bellingham', 98225); 

INSERT INTO Cities(CityName, StateName, Extension, ZipCode)
VALUES('Tri Cities (Kennewick and Pasco and Richland)', 'Washington', 'kpr', 99336); 

INSERT INTO Cities(CityName, StateName, Extension, ZipCode)
VALUES('Lewiston and Clarkston', 'Washington', 'lewiston', 99403); 

INSERT INTO Cities(CityName, StateName, Extension, ZipCode)
VALUES('Moses Lake', 'Washington', 'moseslake', 98837); 

INSERT INTO Cities(CityName, StateName, Extension, ZipCode)
VALUES('Olympic Peninsula', 'Washington', 'olympic', 98362); 

INSERT INTO Cities(CityName, StateName, Extension, ZipCode)
VALUES('Pullman and Moscow', 'Washington', 'pullman', 99163); 

INSERT INTO Cities(CityName, StateName, Extension, ZipCode)
VALUES('Seattle and Tacoma', 'Washington', 'seattle', 98101); 

INSERT INTO Cities(CityName, StateName, Extension, ZipCode)
VALUES('Skagit Island and San Juan Islands', 'Washington', 'skagit', 98232); 

INSERT INTO Cities(CityName, StateName, Extension, ZipCode)
VALUES('Spokane and Coeur dAlene', 'Washington', 'spokane', 98273); 

INSERT INTO Cities(CityName, StateName, Extension, ZipCode)
VALUES('Wenatchee', 'Washington', 'wenatchee', 98801); 

INSERT INTO Cities(CityName, StateName, Extension, ZipCode)
VALUES('Yakima', 'Washington', 'yakima', 98901); 

INSERT INTO Cities(CityName, StateName, Extension, ZipCode)
VALUES('Boise', 'Idaho', 'boise', 83701); 

INSERT INTO Cities(CityName, StateName, Extension, ZipCode)
VALUES('East Idaho', 'Idaho', 'eastidaho', 83401); 

INSERT INTO Cities(CityName, StateName, Extension, ZipCode)
VALUES('Lewiston and Clarkston', 'Idaho', 'lewiston', 83501); 

INSERT INTO Cities(CityName, StateName, Extension, ZipCode)
VALUES('Pullman and Moscow', 'Idaho', 'pullman', 83843); 

INSERT INTO Cities(CityName, StateName, Extension, ZipCode)
VALUES('Spokane and Coeur dAlene', 'Idaho', 'spokane', 83814); 

INSERT INTO Cities(CityName, StateName, Extension, ZipCode)
VALUES('Twin Falls', 'Idaho', 'twinfalls', 83301); 

INSERT INTO Cities(CityName, StateName, Extension, ZipCode)
VALUES('All of Wyoming', 'Wyoming', 'wyoming', 82001); 

INSERT INTO Cities(CityName, StateName, Extension, ZipCode)
VALUES('Logan', 'Utah', 'logan', 84321); 

INSERT INTO Cities(CityName, StateName, Extension, ZipCode)
VALUES('Ogden and Clearfield', 'Utah', 'ogden', 84401); 

INSERT INTO Cities(CityName, StateName, Extension, ZipCode)
VALUES('Provo and Orem', 'Utah', 'provo', 84097); 

INSERT INTO Cities(CityName, StateName, Extension, ZipCode)
VALUES('Salt Lake City', 'Utah', 'saltlakecity', 84044); 

INSERT INTO Cities(CityName, StateName, Extension, ZipCode)
VALUES('St. George', 'Utah', 'stgeorge', 84765); 

INSERT INTO Cities(CityName, StateName, Extension, ZipCode)
VALUES('Lawton', 'Oklahoma', 'lawton', 73501); 

INSERT INTO Cities(CityName, StateName, Extension, ZipCode)
VALUES('Northwest Oklahoma', 'Oklahoma', 'enid', 73717); 

INSERT INTO Cities(CityName, StateName, Extension, ZipCode)
VALUES('Oklahoma City', 'Oklahoma', 'oklahomacity', 73008); 

INSERT INTO Cities(CityName, StateName, Extension, ZipCode)
VALUES('Stillwater', 'Oklahoma', 'stillwater', 74074); 

INSERT INTO Cities(CityName, StateName, Extension, ZipCode)
VALUES('Texoma', 'Oklahoma', 'texoma', 73960); 

INSERT INTO Cities(CityName, StateName, Extension, ZipCode)
VALUES('Tulsa', 'Oklahoma', 'tulsa', 74008); 

INSERT INTO Cities(CityName, StateName, Extension, ZipCode)
VALUES('Albuquerque', 'New Mexico', 'albuquerque', 87101); 

INSERT INTO Cities(CityName, StateName, Extension, ZipCode)
VALUES('Clovis and Portales', 'New Mexico', 'clovis', 88101); 

INSERT INTO Cities(CityName, StateName, Extension, ZipCode)
VALUES('Farmington', 'New Mexico', 'farmington', 87401); 

INSERT INTO Cities(CityName, StateName, Extension, ZipCode)
VALUES('Las Cruces', 'New Mexico', 'lascruces', 88001); 

INSERT INTO Cities(CityName, StateName, Extension, ZipCode)
VALUES('Roswell and Carlsbad', 'New Mexico', 'roswell', 88202); 

INSERT INTO Cities(CityName, StateName, Extension, ZipCode)
VALUES('Santa Fe and Taos', 'New Mexico', 'santafe', 87501); 

INSERT INTO Cities(CityName, StateName, Extension, ZipCode)
VALUES('Flagstaff and Sedona', 'Arizona', 'flagstaff', 86001); 

INSERT INTO Cities(CityName, StateName, Extension, ZipCode)
VALUES('Mohave County', 'Arizona', 'mohave', 86426); 

INSERT INTO Cities(CityName, StateName, Extension, ZipCode)
VALUES('Phoenix', 'Arizona', 'phoenix', 85001); 

INSERT INTO Cities(CityName, StateName, Extension, ZipCode)
VALUES('Prescott', 'Arizona', 'prescott', 86301); 

INSERT INTO Cities(CityName, StateName, Extension, ZipCode)
VALUES('Show Low', 'Arizona', 'showlow', 85901); 

INSERT INTO Cities(CityName, StateName, Extension, ZipCode)
VALUES('Sierra Vista', 'Arizona', 'sierravista', 85613); 

INSERT INTO Cities(CityName, StateName, Extension, ZipCode)
VALUES('Tucson', 'Arizona', 'tucson', 85641); 

INSERT INTO Cities(CityName, StateName, Extension, ZipCode)
VALUES('Yuma', 'Arizona', 'yuma', 85364); 

INSERT INTO Cities(CityName, StateName, Extension, ZipCode)
VALUES('Anchorage and Mat Su Valley', 'Alaska', 'anchorage', 99501); 

INSERT INTO Cities(CityName, StateName, Extension, ZipCode)
VALUES('Fairbanks', 'Alaska', 'fairbanks', 99703); 

INSERT INTO Cities(CityName, StateName, Extension, ZipCode)
VALUES('Kenai Peninsula', 'Alaska', 'kenai', 99611); 

INSERT INTO Cities(CityName, StateName, Extension, ZipCode)
VALUES('Southeast Alaska (Juneau)', 'Alaska', 'juneau', 99801); 

INSERT INTO Cities(CityName, StateName, Extension, ZipCode)
VALUES('All of Hawaii (Honolulu)', 'Hawaii', 'honolulu', 96795); 

INSERT INTO Cities(CityName, StateName, Extension, ZipCode)
VALUES('Washington DC', 'Washington DC', 'washingtondc', 20001); 

INSERT INTO Cities(CityName, StateName, Extension, ZipCode)
VALUES('Bend', 'Oregon', 'bend', 97701); 

INSERT INTO Cities(CityName, StateName, Extension, ZipCode)
VALUES('Corvallis/albany', 'Oregon', 'corvallis', 97330); 

INSERT INTO Cities(CityName, StateName, Extension, ZipCode)
VALUES('East oregon', 'Oregon', 'eastoregon', 97701); 

INSERT INTO Cities(CityName, StateName, Extension, ZipCode)
VALUES('Eugene', 'Oregon', 'eugene', 97401); 

INSERT INTO Cities(CityName, StateName, Extension, ZipCode)
VALUES('Klamath falls', 'Oregon', 'klamath', 97601); 

INSERT INTO Cities(CityName, StateName, Extension, ZipCode)
VALUES('Medford-ashland', 'Oregon', 'medford', 97501); 

INSERT INTO Cities(CityName, StateName, Extension, ZipCode)
VALUES('Oregon coast', 'Oregon', 'oregoncoast', 97367); 

INSERT INTO Cities(CityName, StateName, Extension, ZipCode)
VALUES('Portland', 'Oregon', 'portland', 97035); 

INSERT INTO Cities(CityName, StateName, Extension, ZipCode)
VALUES('Roseburg', 'Oregon', 'roseburg', 97470); 

INSERT INTO Cities(CityName, StateName, Extension, ZipCode)
VALUES('Salem', 'Oregon', 'salem', 97301); 


--Populate Categories Table
INSERT INTO Categories (CategoryName, Extension) Values ('antiques', 'ata');
INSERT INTO Categories (CategoryName, Extension) Values ('applications', 'ppa');
INSERT INTO Categories (CategoryName, Extension) Values ('arts+crafts', 'ara');
INSERT INTO Categories (CategoryName, Extension) Values ('atv/utv/snow', 'sna');
INSERT INTO Categories (CategoryName, Extension) Values ('auto parts (wheels & tires)', 'wta');
INSERT INTO Categories (CategoryName, Extension) Values ('auto parts (general)', 'pta');
INSERT INTO Categories (CategoryName, Extension) Values ('baby+kid', 'baa');
INSERT INTO Categories (CategoryName, Extension) Values ('barter', 'bar');
INSERT INTO Categories (CategoryName, Extension) Values ('beauty+health', 'haa');
INSERT INTO Categories (CategoryName, Extension) Values ('bikes (bicycles)', 'bia');
INSERT INTO Categories (CategoryName, Extension) Values ('bikes (parts & accessories)', 'bip');
INSERT INTO Categories (CategoryName, Extension) Values ('boats (all boats)', 'boo');
INSERT INTO Categories (CategoryName, Extension) Values ('boats (parts & accessories)', 'bpa');
INSERT INTO Categories (CategoryName, Extension) Values ('books', 'bka');
INSERT INTO Categories (CategoryName, Extension) Values ('cars+trucks', 'cta');
INSERT INTO Categories (CategoryName, Extension) Values ('business', 'bip');
INSERT INTO Categories (CategoryName, Extension) Values ('boats (all boats)', 'boo');
INSERT INTO Categories (CategoryName, Extension) Values ('boats (parts & accessories)', 'bpa');
INSERT INTO Categories (CategoryName, Extension) Values ('books', 'bka');
INSERT INTO Categories (CategoryName, Extension) Values ('cars+trucks', 'cta');
INSERT INTO Categories (CategoryName, Extension) Values ('cds/dvd/vhs', 'ema');
INSERT INTO Categories (CategoryName, Extension) Values ('cell phones', 'moa');
INSERT INTO Categories (CategoryName, Extension) Values ('clothes+accessories', 'cla');
INSERT INTO Categories (CategoryName, Extension) Values ('collectibles', 'cba');
INSERT INTO Categories (CategoryName, Extension) Values ('computers (all computers)', 'sya');
INSERT INTO Categories (CategoryName, Extension) Values ('computers (parts & accessories)', 'syp');
INSERT INTO Categories (CategoryName, Extension) Values ('electronics', 'eta');
INSERT INTO Categories (CategoryName, Extension) Values ('farm+garden', 'gra');
INSERT INTO Categories (CategoryName, Extension) Values ('free', 'zip');
INSERT INTO Categories (CategoryName, Extension) Values ('furniture', 'fua');
INSERT INTO Categories (CategoryName, Extension) Values ('garage sales', 'gms');
INSERT INTO Categories (CategoryName, Extension) Values ('general', 'foa');
INSERT INTO Categories (CategoryName, Extension) Values ('heavy equipment', 'hva');
INSERT INTO Categories (CategoryName, Extension) Values ('household', 'hsa');
INSERT INTO Categories (CategoryName, Extension) Values ('jewelry', 'jwa');
INSERT INTO Categories (CategoryName, Extension) Values ('materials', 'maa');
INSERT INTO Categories (CategoryName, Extension) Values ('motorcycles (all motorcycles)', 'mca');
INSERT INTO Categories (CategoryName, Extension) Values ('motorcycles (parts & accessories)', 'mpa');
INSERT INTO Categories (CategoryName, Extension) Values ('music instrument', 'msa');
INSERT INTO Categories (CategoryName, Extension) Values ('photo+video', 'pha');
INSERT INTO Categories (CategoryName, Extension) Values ('rvs+campers', 'rva');
INSERT INTO Categories (CategoryName, Extension) Values ('sporting', 'sga');
INSERT INTO Categories (CategoryName, Extension) Values ('tickets', 'tia');
INSERT INTO Categories (CategoryName, Extension) Values ('tools', 'tla');
INSERT INTO Categories (CategoryName, Extension) Values ('toys+games', 'taa');
INSERT INTO Categories (CategoryName, Extension) Values ('trailers', 'tra');
INSERT INTO Categories (CategoryName, Extension) Values ('video gaming', 'vga');
INSERT INTO Categories (CategoryName, Extension) Values ('wanted', 'waa');

