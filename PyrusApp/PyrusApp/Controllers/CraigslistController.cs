﻿using PyrusApp.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Mvc;
using System.Threading.Tasks;
using System.Diagnostics;
using System.Xml.Linq;
using System.Xml;
using HtmlAgilityPack;
using System.Net;
using System.IO;
using System.Threading;
using System.Collections;

namespace PyrusApp.Controllers
{
    

    [Authorize]
    public class CraigslistController : Controller
    {
        CraigslistContext database = new CraigslistContext();
        //static LinkedList<CraigslistAd> savedAds;


        [HttpGet]
        public ViewResult Search()
        {
            CraigslistSearchViewModel model = new CraigslistSearchViewModel();
            model.Cities = new MultiSelectList(database.Cities.ToList().OrderBy(city => city.StateName), "ID", "CityName", "StateName");
            model.Categories = new MultiSelectList(database.Categories.ToList().OrderBy(category => category.CategoryName), "ID", "CategoryName");
            model.Search = new CraigslistSearch();
            return View(model);
        }

        [HttpPost]
        public ActionResult Search(CraigslistSearch search)
        {
            
            List<String> CityExtensions = new List<String>();
            List<String> CityNames = new List<String>();

            foreach (int cityID in search.Cities)
            {
                CityExtensions.Add(database.Cities.FirstOrDefault(c => c.ID == cityID).Extension);
                CityNames.Add(database.Cities.FirstOrDefault(c => c.ID == cityID).CityName);
            }

            List<String> CategoryExtensions = new List<String>();
            List<String> CategoryNames = new List<String>();

            foreach (int categoryID in search.Categories)
            {
                CategoryExtensions.Add(database.Categories.FirstOrDefault(c => c.ID == categoryID).Extension);
                CategoryNames.Add(database.Categories.FirstOrDefault(c => c.ID == categoryID).CategoryName);
            }

            //Queue of string tuples that define the information parseFeed needs
            Queue<string[]> urlCityCategoryTuples = new Queue<string[]>();
            for (int i = 0; i < CityExtensions.Count; i++)
            {
                for (int j = 0; j < CategoryExtensions.Count; j++)
                {
                    string minPrice = "", maxPrice = "", hasImage = "";

                    if (search.MinPrice != null)
                    {
                        minPrice = "&min_price=" + search.MinPrice;
                    }
                    if (search.MaxPrice != null)
                    {
                        maxPrice = "&max_price=" + search.MaxPrice;
                    }
                    if (search.HasImage)
                    {
                        hasImage = "&hasPic=1";
                    }
                    urlCityCategoryTuples.Enqueue(
                        new string[3] {"https://" + CityExtensions[i] + ".craigslist.org/search/" +
                        CategoryExtensions[j] + "?query=" + search.Query + minPrice + maxPrice + hasImage + "&format=rss",
                        CityNames[i],
                        CategoryNames[j]
                    });

                }
            }

            LinkedList<CraigslistAd> ads = new LinkedList<CraigslistAd>();
            int numUrls = urlCityCategoryTuples.Count;
            //Manage numUrls logic here
            //
            //
            //
            //
            //

            Parallel.For(0, numUrls, index =>
            {
                string[] urlCityCategoryTuple = urlCityCategoryTuples.Dequeue();
                CraigslistUtilities.parseFeed(urlCityCategoryTuple, ads);
            });



            StringBuilder CitiesBuilder = new StringBuilder();
            StringBuilder CategoriesBuilder = new StringBuilder();

            foreach (string city in CityNames)
            {
                CitiesBuilder.Append(city + "\n");
            }

            foreach (string category in CategoryNames)
            {
                CategoriesBuilder.Append(category + "\n");
            }

            ViewBag.CityNames = CitiesBuilder.ToString();
            ViewBag.CategoryNames = CategoriesBuilder.ToString();
            ViewBag.Query = search.Query;

            return PartialView("Results", ads.AsEnumerable<CraigslistAd>());
        }

        /*
        public ActionResult SavedResults()
        {

            return View(savedAds);

        }

        public ActionResult EditNote(string URL)
        {
            var ad = savedAds.Where(item => item.URL == URL).FirstOrDefault();

            return View(ad);
        }

        [HttpPost]
        public ActionResult EditNote(string URL, string Notes)
        {
            savedAds.Where(item => item.URL == URL).FirstOrDefault().Notes = Notes;
            return RedirectToAction("SavedResults", savedAds);
        }
        */
    }
}