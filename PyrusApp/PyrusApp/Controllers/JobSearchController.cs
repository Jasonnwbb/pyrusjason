﻿using PyrusApp.Models;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace PyrusApp.Controllers
{
    [Authorize]
    public class JobSearchController : Controller
    {
        CraigslistContext database = new CraigslistContext();
        // GET: JobSearch
        public ActionResult Search()
        {
            var model = new JobSearchViewModel();
            model.JobSearch = new JobSearch();
            model.Cities = new MultiSelectList(database.Cities.ToList().OrderBy(city => city.StateName), "ID", "CityName", "StateName");

            return View(model);
        }

        // Post: JobSearch
        [HttpPost]
        public ActionResult Search(JobSearch jobSearch)
        {
            int pages = 5;
            jobSearch.Cities = new List<City>();

            var ads = new List<AdType>();

            foreach(var cityID in jobSearch.CityIDs)
            {
                jobSearch.Cities.Add(database.Cities.FirstOrDefault(city => city.ID == cityID));
            }


            List<LocationAndURL> locationsAndURLs = IndeedUtilities.BuildJobSearchBaseURlsWithLocations(jobSearch, Request, pages);
           

            
            foreach (var locationAndURL in locationsAndURLs)
            {

                foreach (var result in IndeedUtilities.Results(locationAndURL.URL))
                {
                    var ad = IndeedUtilities.parseResult(result, locationAndURL.Location);
                    if(ad != null)
                    {
                        ads.Add(ad);
                    }
                    
                }
            }
            

            locationsAndURLs = MonsterUtilities.BuildJobSearchBaseURlsWithLocations(jobSearch, pages);
            foreach (var locationAndURL in locationsAndURLs)
            { 
                foreach (var result in MonsterUtilities.Results(locationAndURL.URL))
                {
                    var ad = MonsterUtilities.parseResult(result, locationAndURL.Location);
                    if(ad != null)
                    {
                        ads.Add(ad);
                    }
                    
                }
            }
            
            List<Tuple<string, string, string>> urlCityCategoryTupleList = new List<Tuple<string,string,string>>();

            foreach(var city in jobSearch.Cities)
            {
                urlCityCategoryTupleList.Add(new Tuple<string, string, string>
                    ("https://" + city.Extension +
                    ".craigslist.org/search/"
                    + "jjj" + "?query=" + jobSearch.Query + "&format=rss", city.CityName,
                    "Jobs"));
            }
            var craigslistAdResults = new LinkedList<CraigslistAd>();
            foreach(var urlCityCategoryTuple in urlCityCategoryTupleList)
            {
                craigslistAdResults = CraigslistUtilities.parseFeed(new string[3] { urlCityCategoryTuple.Item1,
                urlCityCategoryTuple.Item2, urlCityCategoryTuple.Item3}, craigslistAdResults);
            }

            Debug.WriteLine(craigslistAdResults.Count());
            foreach(var craigslistAd in craigslistAdResults)
            {
                ads.Add(new AdType
                {
                    Title = craigslistAd.Title,
                    Network = "Craigslist",
                    Description = craigslistAd.PartialDescription,
                    Date = craigslistAd.TimeStamp,
                    Location = craigslistAd.City,
                    URL = craigslistAd.URL
                });
            }

            return PartialView("Results",ads);

        }
    }
}