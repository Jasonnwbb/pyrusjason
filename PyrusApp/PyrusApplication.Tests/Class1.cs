﻿using System;
using NUnit.Framework;
using System.Reflection;
using System.Web;
using PyrusApp.Models;
using System.Collections.Generic;
namespace Tests
{
    [TestFixture]
    public class TestRoutes
    {
        [Test]
        public void CraigslistBadRequest()
        {
            string[] urlCityCategory =
                new string[3]
                {
                    "toasterpen.com",
                    "Pluto",
                    "Junk"
                };
            LinkedList<CraigslistAd> ads = new LinkedList<CraigslistAd>();
            Assert.That(CraigslistUtilities.parseFeed(urlCityCategory, ads), Is.Null);
        }

        [Test]
        public void NullTestPass()
        {

            Assert.That(null, Is.Null);
        }

        [Test]
        public void AdTypeAreEqual()
        {
            var ad1 = new AdType
            {
                Date = "Now",
                Title = "MyTitle",
                Description = "Blah",
                URL = "www.google.com",
                Location = "99352"

            };

            var ad2 = ad1;

            Assert.That(ad1, Is.EqualTo(ad2));


        }

        [Test]
        public void AdsTypesAreNotEqual()
        {
            var ad1 = new AdType
            {
                Date = "Now",
                Title = "MyTitle",
                Description = "Blah",
                URL = "www.google.com",
                Location = "99352"

            };

            var ad2 = new AdType
            {
                Date = "Then",
                Title = "MyTitle",
                Description = "Blah",
                URL = "www.google.com",
                Location = "99352"

            };

            Assert.AreNotEqual(ad1, ad2);
        }

        [Test]
        public void AssertDifferentTypes()
        {
            var ad = new AdType();
            var craigslistAd = new CraigslistAd();
            Assert.AreNotEqual(ad.GetType(), craigslistAd.GetType());
        }

        [Test]
        public void AssertAdTypeSame()
        {
            var ad = new AdType();
            var ad2 = new AdType();
            Assert.AreEqual(ad.GetType(), ad2.GetType());
        }

        [Test]
        public void AssertAdtypeDoesNotContainPopCornProperty()
        {
            var ad = new AdType();
            var propertyList = ad.GetType().GetProperties();
            bool flag = false;

            foreach(var property in propertyList)
            {
                if(property.Name == "PopCorn")
                {
                    flag = true;
                }
            }
            Assert.False(flag);
        }

        [Test]
        public void AssertCitiesDoesContainTitleProperty()
        {
            var ad = new AdType();
            var propertyList = ad.GetType().GetProperties();
            bool flag = false;

            foreach (var property in propertyList)
            {
                if (property.Name == "Title")
                {
                    flag = true;
                }
            }
            Assert.False(flag);
        }

        [Test]
        public void AssertCraigslistAdDoesNotContainPopCornProperty()
        {
            var ad = new CraigslistAd();
            var propertyList = ad.GetType().GetProperties();
            bool flag = false;

            foreach (var property in propertyList)
            {
                if (property.Name == "PopCorn")
                {
                    flag = true;
                }
            }
            Assert.False(flag);
        }

        [Test]
        public void AssertCraigslistSearchDoesNotContainPopCornProperty()
        {
            var search = new CraigslistSearch();
            var propertyList = search.GetType().GetProperties();
            bool flag = false;

            foreach (var property in propertyList)
            {
                if (property.Name == "PopCorn")
                {
                    flag = true;
                }
            }
            Assert.False(flag);
        }

        [Test]
        public void AssertCityDoesNotContainPopCornProperty()
        {
            var city = new City();
            var propertyList = city.GetType().GetProperties();
            bool flag = false;

            foreach (var property in propertyList)
            {
                if (property.Name == "PopCorn")
                {
                    flag = true;
                }
            }
            Assert.False(flag);
        }

        [Test]
        public void AssertCategoryDoesNotContainPopCornProperty()
        {
            var category = new Category();
            var propertyList = category.GetType().GetProperties();
            bool flag = false;

            foreach (var property in propertyList)
            {
                if (property.Name == "PopCorn")
                {
                    flag = true;
                }
            }
            Assert.False(flag);
        }

        [Test]
        public void AssertJobSearchDoesNotContainPopCornProperty()
        {
            var search = new JobSearch();
            var propertyList = search.GetType().GetProperties();
            bool flag = false;

            foreach (var property in propertyList)
            {
                if (property.Name == "PopCorn")
                {
                    flag = true;
                }
            }
            Assert.False(flag);
        }

        [Test]
        public void CitiesAreNotEqual()
        {
            var city1 = new City
            {
                CityName = "Pasco",
                ID = 1,
                Extension = "kpr",
                StateName = "Washington"
            };

            var city2 = new City
            {
                CityName = "Richland",
                ID = 2,
                Extension = "kpr",
                StateName = "Washington"
            };

            Assert.AreNotEqual(city1, city2);
        }

        [Test]
        public void CategoriesAreNotEqual()
        {
            var category1 = new Category
            {
                CategoryName = "Junk",
                Extension = "junk",
                ID = 1
            };

            var category2 = new Category
            {
                CategoryName = "Computers",
                Extension = "computers",
                ID = 2
            };

            Assert.AreNotEqual(category1, category2);
        }


    }

}
        