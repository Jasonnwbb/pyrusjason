﻿CREATE TABLE SavedAds(
UserID int,
Title varchar(8000),
City varchar(8000),
Category varchar(8000),
URL varchar(8000),
SmallImageURL varchar(8000),
TimeStamp varchar(8000),
Notes varchar(8000)
);