# README


Pyrus is a repository for CS 461 Software Engineering Course at Western Oregon University during Winter Term 2017. The course is a senior course in the Computer Science Program. This repository host resources for the students (i.e. the developers) and the codebase for the main course project: Pyrus Arbitrage.

### Team Members ###

* Jonathan Elliot
* Jiaxin Wang
* Terence Soum

### What is Pyrus Arbitrage? ###

* Web Application for arbitraging through craigslist

### Pyrus Project Vision ###

Our website titled, �Pyrus� is a database driven web application that will offer smart consumer and potential arbitragers the freedom to analyze aggregated data from the Craigslist and Ebay marketplaces. To empower our users we will render easy to interpret opportunity maps and opportunity lists based on a list of type Pyrus and real time data information about our target marketplace. By arbitraging their search items, the users and visitors will have options to optimize their search and then compare prices on those items to decide on whether they are making a good or bad deal. The user will be able to search and filter craigslist to find specific items so that they can find products to buy and later sell for a profit. As a user they will have account settings so they can manage blacklists, personalized auto response systems(ex: special alerts), and see their activity history. Unlike our only one potential competitor, Arbitrage.pro, our solution will be more visually appealing and effective for our users. 

### Status ###
Just started! Development will be ongoing.

### Microsoft Visual Studio 2015 - Community Edition ###
Start up Visual Studio, please confirm it's matching the version 14.0.25431.01 Update 3, Microsoft.NET Framework Version 4.6.01586

### Contributing ###
Contributors, please see this [Guidelines](guidelines.md)



